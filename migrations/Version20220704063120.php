<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20220704063120 extends AbstractMigration
{
    public function getDescription(): string
    {
        return 'Обозначение должности';
    }

    public function up(Schema $schema): void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE positions ADD short_name TEXT NULL');
        $this->addSql('UPDATE positions SET short_name = name');
        $this->addSql('ALTER TABLE positions ALTER short_name SET NOT NULL');
    }

    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE positions DROP short_name');
    }
}
