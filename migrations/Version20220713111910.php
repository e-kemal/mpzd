<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20220713111910 extends AbstractMigration
{
    public function getDescription(): string
    {
        return 'Глобальные классы вместо списка классов в каждой дирекции';
    }

    public function up(Schema $schema): void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE employees DROP CONSTRAINT fk_ba82c3007616678f');
        $this->addSql('ALTER TABLE orders DROP CONSTRAINT fk_e52ffdee332cf8cc');
        $this->addSql('DROP TABLE ranks');
        $this->addSql('DROP INDEX idx_ba82c3007616678f');
        $this->addSql('ALTER TABLE employees ADD rank INT DEFAULT NULL');
        $this->addSql('ALTER TABLE employees DROP rank_id');
        $this->addSql('COMMENT ON COLUMN employees.rank IS \'(DC2Type:rank)\'');
        $this->addSql('DROP INDEX idx_e52ffdee332cf8cc');
        $this->addSql('ALTER TABLE orders ADD author_rank INT DEFAULT NULL');
        $this->addSql('ALTER TABLE orders DROP author_rank_id');
        $this->addSql('COMMENT ON COLUMN orders.author_rank IS \'(DC2Type:rank)\'');
    }

    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('CREATE TABLE ranks (id UUID NOT NULL, directorate_id UUID NOT NULL, name TEXT NOT NULL, created_at TIMESTAMP(0) WITHOUT TIME ZONE NOT NULL, updated_at TIMESTAMP(0) WITHOUT TIME ZONE NOT NULL, PRIMARY KEY(id))');
        $this->addSql('CREATE INDEX idx_cbe6a0149bff530e ON ranks (directorate_id)');
        $this->addSql('COMMENT ON COLUMN ranks.id IS \'(DC2Type:uuid)\'');
        $this->addSql('COMMENT ON COLUMN ranks.directorate_id IS \'(DC2Type:uuid)\'');
        $this->addSql('ALTER TABLE ranks ADD CONSTRAINT fk_cbe6a0149bff530e FOREIGN KEY (directorate_id) REFERENCES directorates (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE employees ADD rank_id UUID DEFAULT NULL');
        $this->addSql('ALTER TABLE employees DROP rank');
        $this->addSql('COMMENT ON COLUMN employees.rank_id IS \'(DC2Type:uuid)\'');
        $this->addSql('ALTER TABLE employees ADD CONSTRAINT fk_ba82c3007616678f FOREIGN KEY (rank_id) REFERENCES ranks (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('CREATE INDEX idx_ba82c3007616678f ON employees (rank_id)');
        $this->addSql('ALTER TABLE orders ADD author_rank_id UUID DEFAULT NULL');
        $this->addSql('ALTER TABLE orders DROP author_rank');
        $this->addSql('COMMENT ON COLUMN orders.author_rank_id IS \'(DC2Type:uuid)\'');
        $this->addSql('ALTER TABLE orders ADD CONSTRAINT fk_e52ffdee332cf8cc FOREIGN KEY (author_rank_id) REFERENCES ranks (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('CREATE INDEX idx_e52ffdee332cf8cc ON orders (author_rank_id)');
    }
}
