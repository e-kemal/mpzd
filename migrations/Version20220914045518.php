<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20220914045518 extends AbstractMigration
{
    public function getDescription(): string
    {
        return 'Баллы';
    }

    public function up(Schema $schema): void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE directorates ADD employees_points_field_id INT NOT NULL DEFAULT 0');
        $this->addSql('ALTER TABLE directorates ALTER employees_points_field_id DROP DEFAULT ');
        $this->addSql('ALTER TABLE employees ADD points INT NOT NULL DEFAULT 0');
        $this->addSql('ALTER TABLE employees ALTER points DROP DEFAULT');
        $this->addSql('ALTER TABLE order_employee_item ADD old_points INT NOT NULL DEFAULT 0');
        $this->addSql('ALTER TABLE order_employee_item ALTER old_points DROP DEFAULT');
        $this->addSql('ALTER TABLE order_employee_item ADD new_points INT NOT NULL DEFAULT 0');
        $this->addSql('ALTER TABLE order_employee_item ALTER new_points DROP DEFAULT');
    }

    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE directorates DROP employees_points_field_id');
        $this->addSql('ALTER TABLE employees DROP points');
        $this->addSql('ALTER TABLE order_employee_item DROP old_points');
        $this->addSql('ALTER TABLE order_employee_item DROP new_points');
    }
}
