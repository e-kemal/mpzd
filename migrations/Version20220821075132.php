<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20220821075132 extends AbstractMigration
{
    public function getDescription(): string
    {
        return 'Табельные номера и погоны';
    }

    public function up(Schema $schema): void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE directorates ADD employees_number_field_id INT NOT NULL DEFAULT 0');
        $this->addSql('ALTER TABLE directorates ALTER employees_number_field_id DROP DEFAULT');
        $this->addSql('ALTER TABLE directorates ADD employees_shoulder_mark_field_id INT NOT NULL DEFAULT 0');
        $this->addSql('ALTER TABLE directorates ALTER employees_shoulder_mark_field_id DROP DEFAULT');
        $this->addSql('ALTER TABLE positions ADD shoulder_mark_file_name VARCHAR(255) NOT NULL DEFAULT \'\'');
        $this->addSql('ALTER TABLE positions ALTER shoulder_mark_file_name DROP DEFAULT');
    }

    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE positions DROP shoulder_mark_file_name');
        $this->addSql('ALTER TABLE directorates DROP employees_number_field_id');
        $this->addSql('ALTER TABLE directorates DROP employees_shoulder_mark_field_id');
    }
}
