<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20230313183751 extends AbstractMigration
{
    public function getDescription(): string
    {
        return 'Допуски';
    }

    public function up(Schema $schema): void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('CREATE TABLE admittances (id UUID NOT NULL, directorate_id UUID NOT NULL, name TEXT NOT NULL, created_at TIMESTAMP(0) WITHOUT TIME ZONE NOT NULL, updated_at TIMESTAMP(0) WITHOUT TIME ZONE NOT NULL, PRIMARY KEY(id))');
        $this->addSql('CREATE INDEX IDX_2C712E429BFF530E ON admittances (directorate_id)');
        $this->addSql('COMMENT ON COLUMN admittances.id IS \'(DC2Type:uuid)\'');
        $this->addSql('COMMENT ON COLUMN admittances.directorate_id IS \'(DC2Type:uuid)\'');
        $this->addSql('ALTER TABLE order_employee_item RENAME TO order_employee_items');
        $this->addSql('ALTER TABLE admittances ADD CONSTRAINT FK_2C712E429BFF530E FOREIGN KEY (directorate_id) REFERENCES directorates (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER INDEX idx_7a1278b8d9f6d38 RENAME TO IDX_616334848D9F6D38');
        $this->addSql('ALTER INDEX idx_7a1278b8c03f15c RENAME TO IDX_616334848C03F15C');
        $this->addSql('ALTER INDEX idx_7a1278b51eb7182 RENAME TO IDX_6163348451EB7182');
        $this->addSql('ALTER INDEX idx_7a1278b4f8b9f3e RENAME TO IDX_616334844F8B9F3E');
    }

    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER INDEX idx_616334844f8b9f3e RENAME TO idx_7a1278b4f8b9f3e');
        $this->addSql('ALTER INDEX idx_6163348451eb7182 RENAME TO idx_7a1278b51eb7182');
        $this->addSql('ALTER INDEX idx_616334848c03f15c RENAME TO idx_7a1278b8c03f15c');
        $this->addSql('ALTER INDEX idx_616334848d9f6d38 RENAME TO idx_7a1278b8d9f6d38');
        $this->addSql('ALTER TABLE order_employee_items RENAME TO order_employee_item');
        $this->addSql('ALTER TABLE admittances DROP CONSTRAINT FK_2C712E429BFF530E');
        $this->addSql('DROP TABLE admittances');
    }
}
