<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20220708160316 extends AbstractMigration
{
    public function getDescription(): string
    {
        return 'Заголовок приказа обязателен';
    }

    public function up(Schema $schema): void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('UPDATE orders SET title = \'\' WHERE title IS NULL');
        $this->addSql('ALTER TABLE orders ALTER title SET NOT NULL');
    }

    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE orders ALTER title DROP NOT NULL');
    }
}
