<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20230314213212 extends AbstractMigration
{
    public function getDescription(): string
    {
        return 'Поле профиля для допусков';
    }

    public function up(Schema $schema): void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE directorates ADD employees_admittances_field_id INT NOT NULL DEFAULT 0');
        $this->addSql('ALTER TABLE directorates ALTER employees_admittances_field_id DROP DEFAULT');
    }

    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE directorates DROP employees_admittances_field_id');
    }
}
