<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20201026150258 extends AbstractMigration
{
    public function getDescription(): string
    {
        return 'oauth';
    }

    public function up(Schema $schema): void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE users ADD external_id INT DEFAULT NULL');
        $this->addSql('ALTER TABLE users ADD oauth_access_token VARCHAR(255) DEFAULT NULL');
        $this->addSql('ALTER TABLE users ADD oauth_expires TIMESTAMP(0) WITHOUT TIME ZONE DEFAULT NULL');
        $this->addSql('ALTER TABLE users ADD oauth_refresh_token VARCHAR(255) DEFAULT NULL');
        $this->addSql('ALTER TABLE users ADD oauth_scopes TEXT DEFAULT NULL');
        $this->addSql('COMMENT ON COLUMN users.oauth_scopes IS \'(DC2Type:simple_array)\'');
    }

    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE users DROP external_id');
        $this->addSql('ALTER TABLE users DROP oauth_access_token');
        $this->addSql('ALTER TABLE users DROP oauth_expires');
        $this->addSql('ALTER TABLE users DROP oauth_refresh_token');
        $this->addSql('ALTER TABLE users DROP oauth_scopes');
    }
}
