<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20220805155610 extends AbstractMigration
{
    public function getDescription(): string
    {
        return 'Кадровые приказы';
    }

    public function up(Schema $schema): void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('CREATE TABLE hr_orders (id UUID NOT NULL, directorate_id UUID NOT NULL, author_id UUID NOT NULL, author_position_id UUID NOT NULL, year INT NOT NULL, number INT DEFAULT NULL, author_rank INT DEFAULT NULL, text TEXT NOT NULL, signed_at TIMESTAMP(0) WITHOUT TIME ZONE DEFAULT NULL, draft_data JSON NOT NULL, external_id INT DEFAULT NULL, external_url TEXT DEFAULT NULL, created_at TIMESTAMP(0) WITHOUT TIME ZONE NOT NULL, updated_at TIMESTAMP(0) WITHOUT TIME ZONE NOT NULL, PRIMARY KEY(id))');
        $this->addSql('CREATE INDEX IDX_6EB764EC9BFF530E ON hr_orders (directorate_id)');
        $this->addSql('CREATE INDEX IDX_6EB764ECF675F31B ON hr_orders (author_id)');
        $this->addSql('CREATE INDEX IDX_6EB764EC81754D3E ON hr_orders (author_position_id)');
        $this->addSql('CREATE UNIQUE INDEX UNIQ_6EB764EC9BFF530EBB82733796901F54 ON hr_orders (directorate_id, year, number)');
        $this->addSql('COMMENT ON COLUMN hr_orders.id IS \'(DC2Type:uuid)\'');
        $this->addSql('COMMENT ON COLUMN hr_orders.directorate_id IS \'(DC2Type:uuid)\'');
        $this->addSql('COMMENT ON COLUMN hr_orders.author_id IS \'(DC2Type:uuid)\'');
        $this->addSql('COMMENT ON COLUMN hr_orders.author_position_id IS \'(DC2Type:uuid)\'');
        $this->addSql('COMMENT ON COLUMN hr_orders.author_rank IS \'(DC2Type:rank)\'');
        $this->addSql('COMMENT ON COLUMN hr_orders.signed_at IS \'(DC2Type:datetime_immutable)\'');
        $this->addSql('CREATE TABLE order_employee_item (id UUID NOT NULL, order_id UUID NOT NULL, employee_id UUID NOT NULL, old_position_id UUID DEFAULT NULL, new_position_id UUID DEFAULT NULL, old_rank INT DEFAULT NULL, new_rank INT DEFAULT NULL, created_at TIMESTAMP(0) WITHOUT TIME ZONE NOT NULL, updated_at TIMESTAMP(0) WITHOUT TIME ZONE NOT NULL, PRIMARY KEY(id))');
        $this->addSql('CREATE INDEX IDX_7A1278B8D9F6D38 ON order_employee_item (order_id)');
        $this->addSql('CREATE INDEX IDX_7A1278B8C03F15C ON order_employee_item (employee_id)');
        $this->addSql('CREATE INDEX IDX_7A1278B51EB7182 ON order_employee_item (old_position_id)');
        $this->addSql('CREATE INDEX IDX_7A1278B4F8B9F3E ON order_employee_item (new_position_id)');
        $this->addSql('COMMENT ON COLUMN order_employee_item.id IS \'(DC2Type:uuid)\'');
        $this->addSql('COMMENT ON COLUMN order_employee_item.order_id IS \'(DC2Type:uuid)\'');
        $this->addSql('COMMENT ON COLUMN order_employee_item.employee_id IS \'(DC2Type:uuid)\'');
        $this->addSql('COMMENT ON COLUMN order_employee_item.old_position_id IS \'(DC2Type:uuid)\'');
        $this->addSql('COMMENT ON COLUMN order_employee_item.new_position_id IS \'(DC2Type:uuid)\'');
        $this->addSql('COMMENT ON COLUMN order_employee_item.old_rank IS \'(DC2Type:rank)\'');
        $this->addSql('COMMENT ON COLUMN order_employee_item.new_rank IS \'(DC2Type:rank)\'');
        $this->addSql('ALTER TABLE hr_orders ADD CONSTRAINT FK_6EB764EC9BFF530E FOREIGN KEY (directorate_id) REFERENCES directorates (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE hr_orders ADD CONSTRAINT FK_6EB764ECF675F31B FOREIGN KEY (author_id) REFERENCES employees (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE hr_orders ADD CONSTRAINT FK_6EB764EC81754D3E FOREIGN KEY (author_position_id) REFERENCES positions (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE order_employee_item ADD CONSTRAINT FK_7A1278B8D9F6D38 FOREIGN KEY (order_id) REFERENCES hr_orders (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE order_employee_item ADD CONSTRAINT FK_7A1278B8C03F15C FOREIGN KEY (employee_id) REFERENCES employees (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE order_employee_item ADD CONSTRAINT FK_7A1278B51EB7182 FOREIGN KEY (old_position_id) REFERENCES positions (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE order_employee_item ADD CONSTRAINT FK_7A1278B4F8B9F3E FOREIGN KEY (new_position_id) REFERENCES positions (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE directorates ADD hr_orders_topic_id INT NOT NULL DEFAULT 0');
        $this->addSql('ALTER TABLE directorates ALTER hr_orders_topic_id DROP DEFAULT');
    }

    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE order_employee_item DROP CONSTRAINT FK_7A1278B8D9F6D38');
        $this->addSql('DROP TABLE hr_orders');
        $this->addSql('DROP TABLE order_employee_item');
        $this->addSql('ALTER TABLE directorates DROP hr_orders_topic_id');
    }
}
