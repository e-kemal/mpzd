<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20201027151058 extends AbstractMigration
{
    public function getDescription(): string
    {
        return 'Дирекции, работники, должности, классы';
    }

    public function up(Schema $schema): void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('CREATE TABLE directorates (id UUID NOT NULL, name TEXT NOT NULL, created_at TIMESTAMP(0) WITHOUT TIME ZONE NOT NULL, updated_at TIMESTAMP(0) WITHOUT TIME ZONE NOT NULL, PRIMARY KEY(id))');
        $this->addSql('COMMENT ON COLUMN directorates.id IS \'(DC2Type:uuid)\'');
        $this->addSql('CREATE TABLE employees (id UUID NOT NULL, user_id UUID NOT NULL, directorate_id UUID NOT NULL, position_id UUID DEFAULT NULL, rank_id UUID DEFAULT NULL, number INT NOT NULL, created_at TIMESTAMP(0) WITHOUT TIME ZONE NOT NULL, updated_at TIMESTAMP(0) WITHOUT TIME ZONE NOT NULL, PRIMARY KEY(id))');
        $this->addSql('CREATE INDEX IDX_BA82C300A76ED395 ON employees (user_id)');
        $this->addSql('CREATE INDEX IDX_BA82C3009BFF530E ON employees (directorate_id)');
        $this->addSql('CREATE INDEX IDX_BA82C300DD842E46 ON employees (position_id)');
        $this->addSql('CREATE INDEX IDX_BA82C3007616678F ON employees (rank_id)');
        $this->addSql('CREATE UNIQUE INDEX UNIQ_BA82C300A76ED3959BFF530E ON employees (user_id, directorate_id)');
        $this->addSql('CREATE UNIQUE INDEX UNIQ_BA82C3009BFF530E96901F54 ON employees (directorate_id, number)');
        $this->addSql('COMMENT ON COLUMN employees.id IS \'(DC2Type:uuid)\'');
        $this->addSql('COMMENT ON COLUMN employees.user_id IS \'(DC2Type:uuid)\'');
        $this->addSql('COMMENT ON COLUMN employees.directorate_id IS \'(DC2Type:uuid)\'');
        $this->addSql('COMMENT ON COLUMN employees.position_id IS \'(DC2Type:uuid)\'');
        $this->addSql('COMMENT ON COLUMN employees.rank_id IS \'(DC2Type:uuid)\'');
        $this->addSql('CREATE TABLE positions (id UUID NOT NULL, directorate_id UUID NOT NULL, name TEXT NOT NULL, access_level SMALLINT NOT NULL, created_at TIMESTAMP(0) WITHOUT TIME ZONE NOT NULL, updated_at TIMESTAMP(0) WITHOUT TIME ZONE NOT NULL, PRIMARY KEY(id))');
        $this->addSql('CREATE INDEX IDX_D69FE57C9BFF530E ON positions (directorate_id)');
        $this->addSql('COMMENT ON COLUMN positions.id IS \'(DC2Type:uuid)\'');
        $this->addSql('COMMENT ON COLUMN positions.directorate_id IS \'(DC2Type:uuid)\'');
        $this->addSql('CREATE TABLE ranks (id UUID NOT NULL, directorate_id UUID NOT NULL, name TEXT NOT NULL, created_at TIMESTAMP(0) WITHOUT TIME ZONE NOT NULL, updated_at TIMESTAMP(0) WITHOUT TIME ZONE NOT NULL, PRIMARY KEY(id))');
        $this->addSql('CREATE INDEX IDX_CBE6A0149BFF530E ON ranks (directorate_id)');
        $this->addSql('COMMENT ON COLUMN ranks.id IS \'(DC2Type:uuid)\'');
        $this->addSql('COMMENT ON COLUMN ranks.directorate_id IS \'(DC2Type:uuid)\'');
        $this->addSql('ALTER TABLE employees ADD CONSTRAINT FK_BA82C300A76ED395 FOREIGN KEY (user_id) REFERENCES users (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE employees ADD CONSTRAINT FK_BA82C3009BFF530E FOREIGN KEY (directorate_id) REFERENCES directorates (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE employees ADD CONSTRAINT FK_BA82C300DD842E46 FOREIGN KEY (position_id) REFERENCES positions (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE employees ADD CONSTRAINT FK_BA82C3007616678F FOREIGN KEY (rank_id) REFERENCES ranks (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE positions ADD CONSTRAINT FK_D69FE57C9BFF530E FOREIGN KEY (directorate_id) REFERENCES directorates (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE ranks ADD CONSTRAINT FK_CBE6A0149BFF530E FOREIGN KEY (directorate_id) REFERENCES directorates (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE users ALTER oauth_access_token TYPE TEXT');
        $this->addSql('ALTER TABLE users ALTER oauth_access_token DROP DEFAULT');
        $this->addSql('ALTER TABLE users ALTER oauth_refresh_token TYPE TEXT');
        $this->addSql('ALTER TABLE users ALTER oauth_refresh_token DROP DEFAULT');
    }

    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE employees DROP CONSTRAINT FK_BA82C3009BFF530E');
        $this->addSql('ALTER TABLE positions DROP CONSTRAINT FK_D69FE57C9BFF530E');
        $this->addSql('ALTER TABLE ranks DROP CONSTRAINT FK_CBE6A0149BFF530E');
        $this->addSql('ALTER TABLE employees DROP CONSTRAINT FK_BA82C300DD842E46');
        $this->addSql('ALTER TABLE employees DROP CONSTRAINT FK_BA82C3007616678F');
        $this->addSql('DROP TABLE directorates');
        $this->addSql('DROP TABLE employees');
        $this->addSql('DROP TABLE positions');
        $this->addSql('DROP TABLE ranks');
        $this->addSql('ALTER TABLE users ALTER oauth_access_token TYPE VARCHAR(255)');
        $this->addSql('ALTER TABLE users ALTER oauth_access_token DROP DEFAULT');
        $this->addSql('ALTER TABLE users ALTER oauth_refresh_token TYPE VARCHAR(255)');
        $this->addSql('ALTER TABLE users ALTER oauth_refresh_token DROP DEFAULT');
    }
}
