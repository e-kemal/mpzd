<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20220705152519 extends AbstractMigration
{
    public function getDescription(): string
    {
        return 'Экспорт приказов на форум';
    }

    public function up(Schema $schema): void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE directorates ADD orders_topic_id INT NOT NULL DEFAULT 0');
        $this->addSql('ALTER TABLE directorates ALTER orders_topic_id DROP DEFAULT');
        $this->addSql('ALTER TABLE orders ADD external_id INT DEFAULT NULL');
        $this->addSql('ALTER TABLE orders ADD external_url TEXT DEFAULT NULL');
    }

    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE orders DROP external_id');
        $this->addSql('ALTER TABLE orders DROP external_url');
        $this->addSql('ALTER TABLE directorates DROP orders_topic_id');
    }
}
