<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20230314202032 extends AbstractMigration
{
    public function getDescription(): string
    {
        return 'Допуски в приказах';
    }

    public function up(Schema $schema): void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE order_employee_items ADD add_admittance_ids JSON NOT NULL DEFAULT \'[]\'');
        $this->addSql('ALTER TABLE order_employee_items ALTER add_admittance_ids DROP DEFAULT ');
        $this->addSql('ALTER TABLE order_employee_items ADD remove_admittance_ids JSON NOT NULL DEFAULT \'[]\'');
        $this->addSql('ALTER TABLE order_employee_items ALTER remove_admittance_ids DROP DEFAULT ');
        $this->addSql('ALTER TABLE order_employee_items ADD actual_admittance_ids JSON NOT NULL DEFAULT \'[]\'');
        $this->addSql('ALTER TABLE order_employee_items ALTER actual_admittance_ids DROP DEFAULT ');
    }

    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE order_employee_items DROP add_admittance_ids');
        $this->addSql('ALTER TABLE order_employee_items DROP remove_admittance_ids');
        $this->addSql('ALTER TABLE order_employee_items DROP actual_admittance_ids');
    }
}
