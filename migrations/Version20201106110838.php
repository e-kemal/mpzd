<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20201106110838 extends AbstractMigration
{
    public function getDescription(): string
    {
        return 'Обозначение, алиас и флаг корневой дирекции';
    }

    public function up(Schema $schema): void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE directorates ADD short_name TEXT DEFAULT NULL');
        $this->addSql('ALTER TABLE directorates ADD alias TEXT DEFAULT NULL');
        $this->addSql('ALTER TABLE directorates ADD root BOOLEAN DEFAULT \'false\' NOT NULL');
        $this->addSql('CREATE UNIQUE INDEX UNIQ_A2471A25E16C6B94 ON directorates (alias)');
    }

    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('DROP INDEX UNIQ_A2471A25E16C6B94');
        $this->addSql('ALTER TABLE directorates DROP short_name');
        $this->addSql('ALTER TABLE directorates DROP alias');
        $this->addSql('ALTER TABLE directorates DROP root');
    }
}
