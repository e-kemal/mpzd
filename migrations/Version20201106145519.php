<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20201106145519 extends AbstractMigration
{
    public function getDescription(): string
    {
        return 'not null для обозначения и алиаса дирекций';
    }

    public function up(Schema $schema): void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE directorates ALTER short_name SET NOT NULL');
        $this->addSql('ALTER TABLE directorates ALTER alias SET NOT NULL');
    }

    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE directorates ALTER short_name DROP NOT NULL');
        $this->addSql('ALTER TABLE directorates ALTER alias DROP NOT NULL');
    }
}
