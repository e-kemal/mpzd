<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20230313204229 extends AbstractMigration
{
    public function getDescription(): string
    {
        return 'Допуски пользователей';
    }

    public function up(Schema $schema): void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('CREATE TABLE employee_admittances (employee_id UUID NOT NULL, admittance_id UUID NOT NULL, PRIMARY KEY(employee_id, admittance_id))');
        $this->addSql('CREATE INDEX IDX_AD57EF3F8C03F15C ON employee_admittances (employee_id)');
        $this->addSql('CREATE INDEX IDX_AD57EF3F90F7D4ED ON employee_admittances (admittance_id)');
        $this->addSql('COMMENT ON COLUMN employee_admittances.employee_id IS \'(DC2Type:uuid)\'');
        $this->addSql('COMMENT ON COLUMN employee_admittances.admittance_id IS \'(DC2Type:uuid)\'');
        $this->addSql('ALTER TABLE employee_admittances ADD CONSTRAINT FK_AD57EF3F8C03F15C FOREIGN KEY (employee_id) REFERENCES employees (id) ON DELETE CASCADE NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE employee_admittances ADD CONSTRAINT FK_AD57EF3F90F7D4ED FOREIGN KEY (admittance_id) REFERENCES admittances (id) ON DELETE CASCADE NOT DEFERRABLE INITIALLY IMMEDIATE');
    }

    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE employee_admittances DROP CONSTRAINT FK_AD57EF3F8C03F15C');
        $this->addSql('ALTER TABLE employee_admittances DROP CONSTRAINT FK_AD57EF3F90F7D4ED');
        $this->addSql('DROP TABLE employee_admittances');
    }
}
