<?php

declare(strict_types=1);

namespace App\MessageHandler;

use App\Message\UpdateEmployeeFields;
use App\Repository\EmployeeRepository;
use App\Service\ForumService;
use Symfony\Component\Messenger\Attribute\AsMessageHandler;

#[AsMessageHandler]
final class UpdateEmployeeFieldsHandler
{
    public function __construct(
        private EmployeeRepository $employeeRepository,
        private ForumService $forumService,
    ) {
    }

    /**
     * @throws \Exception
     */
    public function __invoke(UpdateEmployeeFields $message)
    {
        $employee = $this->employeeRepository->find($message->getEmployeeId());
        if (!$employee) {
            throw new \Exception();
        }

        $fields = [];
        if ($employee->getDirectorate()->getEmployeesNumberFieldId()) {
            $fields[$employee->getDirectorate()->getEmployeesNumberFieldId()] = $employee->getNumber();
        }
        if ($employee->getDirectorate()->getEmployeesShoulderMarkFieldId()) {
            $filename = $employee->getPosition()?->getShoulderMarkFileName();
            if (null !== $filename) {
                $filename = str_replace('%rank%', (string) $employee->getRank(), $filename);
            }
            $fields[$employee->getDirectorate()->getEmployeesShoulderMarkFieldId()] = $filename ?? '';
        }
        if ($employee->getDirectorate()->getEmployeesPointsFieldId()) {
            $fields[$employee->getDirectorate()->getEmployeesPointsFieldId()] = $employee->getPoints();
        }
        if ($employee->getDirectorate()->getEmployeesAdmittancesFieldId()) {
            $fields[$employee->getDirectorate()->getEmployeesAdmittancesFieldId()] = implode(', ', $employee->getAdmittances());
        }
        $this->forumService->updateCustomFields($employee->getUser()->getExternalId(), $fields);
    }
}
