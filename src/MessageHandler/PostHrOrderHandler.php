<?php

declare(strict_types=1);

namespace App\MessageHandler;

use App\Message\PostHrOrder;
use App\Repository\HrOrderRepository;
use App\Service\ForumService;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\Messenger\Attribute\AsMessageHandler;
use Twig\Environment;

#[AsMessageHandler]
final class PostHrOrderHandler
{
    public function __construct(
        private HrOrderRepository $orderRepository,
        private ForumService $forumService,
        private EntityManagerInterface $entityManager,
        private Environment $twig,
    ) {
    }

    /**
     * @throws \Exception
     */
    public function __invoke(PostHrOrder $message)
    {
        $order = $this->orderRepository->find($message->getOrderId());
        if (!$order || null === $order->getNumber()) {
            throw new \Exception();
        }
        if (null !== $order->getExternalId() || null !== $order->getExternalUrl()) {
            return;
        }

        $forumData = $this->forumService->post(
            $order->getDirectorate()->getHrOrdersTopicId(),
            $order->getAuthor()->getUser()->getExternalId(),
            $this->twig->render('hr_order/export.html.twig', ['order' => $order]),
        );
        $order
            ->setExternalId($forumData['id'])
            ->setExternalUrl($forumData['url'])
        ;
        $this->entityManager->flush();
    }
}
