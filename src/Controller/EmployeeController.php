<?php

declare(strict_types=1);

namespace App\Controller;

use App\Entity\Directorate;
use App\Repository\EmployeeRepository;
use App\Repository\OrderEmployeeItemRepository;
use Symfony\Bridge\Doctrine\Attribute\MapEntity;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Symfony\Component\Routing\Annotation\Route;

#[Route('/d/{directorateAlias}/employees')]
class EmployeeController extends AbstractController
{
    #[Route('/')]
    public function index(
        Request $request,
        EmployeeRepository $employeeRepository,
        #[MapEntity(mapping: ['directorateAlias' => 'alias'])]
        Directorate $directorate
    ): Response {
        $employees = $employeeRepository->paginateByDirectorate($directorate, $request->query->getInt('page', 1));

        return $this->render('employee/index.html.twig', [
            'directorate' => $directorate,
            'employees' => $employees,
        ]);
    }

    #[Route('/{number}')]
    public function show(
        EmployeeRepository $employeeRepository,
        OrderEmployeeItemRepository $orderEmployeeItemRepository,
        #[MapEntity(mapping: ['directorateAlias' => 'alias'])]
        Directorate $directorate,
        int $number,
    ): Response {
        $employee = $employeeRepository->findOneBy(['directorate' => $directorate, 'number' => $number]);
        if (!$employee) {
            throw new NotFoundHttpException();
        }

        return $this->render('employee/show.html.twig', [
            'directorate' => $directorate,
            'employee' => $employee,
            'orderItems' => $orderEmployeeItemRepository->findItemsByEmployee($employee),
        ]);
    }
}
