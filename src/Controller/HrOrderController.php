<?php

declare(strict_types=1);

namespace App\Controller;

use App\Entity\Directorate;
use App\Repository\HrOrderRepository;
use App\Repository\OrderEmployeeItemRepository;
use Symfony\Bridge\Doctrine\Attribute\MapEntity;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Symfony\Component\Routing\Annotation\Route;

#[Route('/d/{directorateAlias}/hr-orders')]
class HrOrderController extends AbstractController
{
    #[Route('/')]
    public function index(
        Request $request,
        HrOrderRepository $orderRepository,
        #[MapEntity(mapping: ['directorateAlias' => 'alias'])]
        Directorate $directorate
    ): Response {
        $orders = $orderRepository->paginateByDirectorate($directorate, page: $request->query->getInt('page', 1));

        return $this->render('hr_order/index.html.twig', [
            'directorate' => $directorate,
            'orders' => $orders,
        ]);
    }

    #[Route('/{year}/{number}')]
    public function show(
        HrOrderRepository $orderRepository,
        OrderEmployeeItemRepository $orderEmployeeItemRepository,
        #[MapEntity(mapping: ['directorateAlias' => 'alias'])]
        Directorate $directorate,
        int $year,
        int $number
    ): Response {
        $order = $orderRepository->findOneBy(['directorate' => $directorate, 'year' => $year, 'number' => $number]);
        if (!$order) {
            throw new NotFoundHttpException();
        }

        return $this->render('hr_order/show.html.twig', [
            'directorate' => $directorate,
            'order' => $order,
            'employeeItems' => $orderEmployeeItemRepository->findItemsByOrder($order),
        ]);
    }
}
