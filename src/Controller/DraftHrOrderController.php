<?php

declare(strict_types=1);

namespace App\Controller;

use App\Entity\Directorate;
use App\Entity\HrOrder;
use App\Form\DraftHrOrderType;
use App\Repository\EmployeeRepository;
use App\Repository\HrOrderRepository;
use App\Service\DraftHrOrderService;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bridge\Doctrine\Attribute\MapEntity;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Security\Core\Exception\AccessDeniedException;

#[Route('/d/{directorateAlias}/draft-hr-orders')]
class DraftHrOrderController extends AbstractController
{
    #[Route('/', methods: ['GET'])]
    public function index(
        Request $request,
        HrOrderRepository $hrOrderRepository,
        #[MapEntity(mapping: ['directorateAlias' => 'alias'])]
        Directorate $directorate
    ): Response {
        $this->denyAccessUnlessGranted('DRAFT_ORDER_LIST', $directorate);
        $hrOrders = $hrOrderRepository->paginateByDirectorate($directorate, false, $request->query->getInt('page', 1));

        return $this->render('draft_hr_order/index.html.twig', [
            'directorate' => $directorate,
            'hr_orders' => $hrOrders,
        ]);
    }

    #[Route('/new', methods: ['GET', 'POST'])]
    public function new(
        Request $request,
        EmployeeRepository $employeeRepository,
        #[MapEntity(mapping: ['directorateAlias' => 'alias'])]
        Directorate $directorate,
        DraftHrOrderService $draftHrOrderService,
    ): Response {
        $this->denyAccessUnlessGranted('DRAFT_ORDER_CREATE', $directorate);
        $employee = $employeeRepository->findOneBy(['directorate' => $directorate, 'user' => $this->getUser()]);
        if (!$employee) {
            throw new AccessDeniedException();
        }
        $draft = $draftHrOrderService->makeNewDto($employee);
        $form = $this->createForm(DraftHrOrderType::class, $draft, ['directorate' => $employee->getDirectorate()]);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $sign = null !== $request->get('btn_sign');
            $draftHrOrderService->applyDto($draft, $sign, $employee);

            $hrOrder = $draft->getOrder();
            if (null !== $hrOrder->getNumber()) {
                return $this->redirectToRoute('app_hrorder_show', [
                    'directorateAlias' => $hrOrder->getDirectorate()->getAlias(),
                    'year' => $hrOrder->getYear(),
                    'number' => $hrOrder->getNumber(),
                ], Response::HTTP_SEE_OTHER);
            }

            return $this->redirectToRoute('app_drafthrorder_index', ['directorateAlias' => $directorate->getAlias()], Response::HTTP_SEE_OTHER);
        }

        return $this->render('draft_hr_order/new.html.twig', [
            'hr_order' => $draft->getOrder(),
            'form' => $form,
        ]);
    }

    #[Route('/{id}', methods: ['GET'])]
    public function show(HrOrder $hrOrder, DraftHrOrderService $draftHrOrderService): Response
    {
        if (null !== $hrOrder->getNumber()) {
            return $this->redirectToRoute('app_hrorder_show', [
                'directorateAlias' => $hrOrder->getDirectorate()->getAlias(),
                'year' => $hrOrder->getYear(),
                'number' => $hrOrder->getNumber(),
            ], Response::HTTP_MOVED_PERMANENTLY);
        }
        $this->denyAccessUnlessGranted('DRAFT_ORDER_SHOW', $hrOrder);

        return $this->render('draft_hr_order/show.html.twig', [
            'hr_order' => $hrOrder,
            'draft' => $draftHrOrderService->makeDtoByOrder($hrOrder),
        ]);
    }

    #[Route('/{id}/edit', methods: ['GET', 'POST'])]
    public function edit(
        Request $request,
        EmployeeRepository $employeeRepository,
        HrOrder $hrOrder,
        DraftHrOrderService $draftHrOrderService,
    ): Response {
        $this->denyAccessUnlessGranted('DRAFT_ORDER_EDIT', $hrOrder);
        $employee = $employeeRepository->findOneBy(['directorate' => $hrOrder->getDirectorate(), 'user' => $this->getUser()]);
        $draft = $draftHrOrderService->makeDtoByOrder($hrOrder);
        $form = $this->createForm(DraftHrOrderType::class, $draft, ['directorate' => $hrOrder->getDirectorate()]);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid() && null === $hrOrder->getNumber()) {
            $sign = null !== $request->get('btn_sign');
            $draftHrOrderService->applyDto($draft, $sign, $employee);

            if (null !== $hrOrder->getNumber()) {
                return $this->redirectToRoute('app_hrorder_show', [
                    'directorateAlias' => $hrOrder->getDirectorate()->getAlias(),
                    'year' => $hrOrder->getYear(),
                    'number' => $hrOrder->getNumber(),
                ], Response::HTTP_SEE_OTHER);
            }

            return $this->redirectToRoute('app_drafthrorder_index', [
                'directorateAlias' => $hrOrder->getDirectorate()->getAlias(),
            ], Response::HTTP_SEE_OTHER);
        }

        return $this->render('draft_hr_order/edit.html.twig', [
            'hr_order' => $hrOrder,
            'form' => $form,
        ]);
    }

    #[Route('/{id}', methods: ['POST'])]
    public function delete(Request $request, HrOrder $hrOrder, EntityManagerInterface $entityManager): Response
    {
        $this->denyAccessUnlessGranted('DRAFT_ORDER_DELETE', $hrOrder);
        $directorateAlias = $hrOrder->getDirectorate()->getAlias();
        if ($this->isCsrfTokenValid('delete'.$hrOrder->getId(), $request->request->get('_token')) && null === $hrOrder->getNumber()) {
            $entityManager->remove($hrOrder);
            $entityManager->flush();
        }

        return $this->redirectToRoute('app_drafthrorder_index', ['directorateAlias' => $directorateAlias], Response::HTTP_SEE_OTHER);
    }
}
