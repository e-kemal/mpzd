<?php

declare(strict_types=1);

namespace App\Controller;

use App\Entity\Directorate;
use App\Repository\OrderRepository;
use Symfony\Bridge\Doctrine\Attribute\MapEntity;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Symfony\Component\Routing\Annotation\Route;

#[Route('/d/{directorateAlias}/orders')]
class OrderController extends AbstractController
{
    #[Route('/')]
    public function index(
        Request $request,
        OrderRepository $orderRepository,
        #[MapEntity(mapping: ['directorateAlias' => 'alias'])]
        Directorate $directorate
    ): Response {
        $orders = $orderRepository->paginateByDirectorate($directorate, page: $request->query->getInt('page', 1));

        return $this->render('order/index.html.twig', [
            'directorate' => $directorate,
            'orders' => $orders,
        ]);
    }

    #[Route('/{number}', requirements: ['number' => '\d+'])]
    public function show(
        OrderRepository $orderRepository,
        #[MapEntity(mapping: ['directorateAlias' => 'alias'])]
        Directorate $directorate,
        int $number
    ): Response {
        $order = $orderRepository->findOneBy(['directorate' => $directorate, 'number' => $number]);
        if (!$order) {
            throw new NotFoundHttpException();
        }

        return $this->render('order/show.html.twig', [
            'directorate' => $directorate,
            'order' => $order,
        ]);
    }

    #[Route('/autocomplete')]
    public function autocomplete(
        Request $request,
        OrderRepository $orderRepository,
        #[MapEntity(mapping: ['directorateAlias' => 'alias'])]
        Directorate $directorate
    ): JsonResponse {
        $result = [];
        $more = false;
        $q = $request->get('q', '');
        $page = $request->query->getInt('page', 1);
        if ($directorate->isRoot()) {
            $directorate = null;
        }
        if (!empty($q)) {
            $orders = $orderRepository->findByDirectorateAndQuery($directorate, $q, 10 * ($page - 1), 11);
            if (count($orders) > 10) {
                $more = true;
            }
            foreach ($orders as $order) {
                $result[] = [
                    'id' => $order->getId(),
                    'text' => sprintf('%s %s', $order, $order->getTitle()),
                ];
            }
        }

        return new JsonResponse([
            'results' => $result,
            'pagination' => ['more' => $more],
        ]);
    }
}
