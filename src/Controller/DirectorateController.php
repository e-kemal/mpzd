<?php

declare(strict_types=1);

namespace App\Controller;

use App\Entity\Directorate;
use App\Repository\EmployeeRepository;
use App\Repository\HrOrderRepository;
use App\Repository\OrderRepository;
use Symfony\Bridge\Doctrine\Attribute\MapEntity;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

#[Route('/d/{directorateAlias}')]
class DirectorateController extends AbstractController
{
    #[Route('/')]
    public function index(
        #[MapEntity(mapping: ['directorateAlias' => 'alias'])]
        Directorate $directorate,
        EmployeeRepository $employeeRepository,
        OrderRepository $orderRepository,
        HrOrderRepository $hrOrderRepository,
    ): Response {
        return $this->render('directorate/index.html.twig', [
            'directorate' => $directorate,
            'leaders' => $employeeRepository->findLeadersByDirectorate($directorate),
            'orders' => $orderRepository->findLatestByDirectorate($directorate),
            'hr_orders' => $hrOrderRepository->findLatestByDirectorate($directorate),
        ]);
    }
}
