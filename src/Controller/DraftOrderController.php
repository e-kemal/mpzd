<?php

declare(strict_types=1);

namespace App\Controller;

use App\Entity\Directorate;
use App\Entity\Order;
use App\Form\DraftOrderType;
use App\Repository\EmployeeRepository;
use App\Repository\OrderRepository;
use App\Service\DraftOrderService;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bridge\Doctrine\Attribute\MapEntity;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Security\Core\Exception\AccessDeniedException;

#[Route('/d/{directorateAlias}/draft-orders')]
class DraftOrderController extends AbstractController
{
    #[Route('/', methods: ['GET'])]
    public function index(
        Request $request,
        OrderRepository $orderRepository,
        #[MapEntity(mapping: ['directorateAlias' => 'alias'])]
        Directorate $directorate
    ): Response {
        $this->denyAccessUnlessGranted('DRAFT_ORDER_LIST', $directorate);
        $orders = $orderRepository->paginateByDirectorate($directorate, false, $request->query->getInt('page', 1));

        return $this->render('draft_order/index.html.twig', [
            'directorate' => $directorate,
            'orders' => $orders,
        ]);
    }

    #[Route('/new', methods: ['GET', 'POST'])]
    public function new(
        Request $request,
        EmployeeRepository $employeeRepository,
        #[MapEntity(mapping: ['directorateAlias' => 'alias'])]
        Directorate $directorate,
        DraftOrderService $draftOrderService,
    ): Response {
        $this->denyAccessUnlessGranted('DRAFT_ORDER_CREATE', $directorate);
        $employee = $employeeRepository->findOneBy(['directorate' => $directorate, 'user' => $this->getUser()]);
        if (!$employee) {
            throw new AccessDeniedException();
        }
        $draft = $draftOrderService->makeNewDto($employee);
        $form = $this->createForm(DraftOrderType::class, $draft);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $sign = null !== $request->get('btn_sign');
            $draftOrderService->applyDto($draft, $sign, $employee);

            $order = $draft->getOrder();
            if (null !== $order->getNumber()) {
                return $this->redirectToRoute('app_order_show', [
                    'directorateAlias' => $order->getDirectorate()->getAlias(),
                    'number' => $order->getNumber(),
                ], Response::HTTP_SEE_OTHER);
            }

            return $this->redirectToRoute('app_draftorder_index', ['directorateAlias' => $directorate->getAlias()], Response::HTTP_SEE_OTHER);
        }

        return $this->render('draft_order/new.html.twig', [
            'order' => $draft->getOrder(),
            'form' => $form,
        ]);
    }

    #[Route('/{id}', methods: ['GET'])]
    public function show(Order $order, DraftOrderService $draftOrderService): Response
    {
        if (null !== $order->getNumber()) {
            return $this->redirectToRoute('app_order_show', [
                'directorateAlias' => $order->getDirectorate()->getAlias(),
                'number' => $order->getNumber(),
            ], Response::HTTP_MOVED_PERMANENTLY);
        }
        $this->denyAccessUnlessGranted('DRAFT_ORDER_SHOW', $order);

        return $this->render('draft_order/show.html.twig', [
            'order' => $order,
            'draft' => $draftOrderService->makeDtoByOrder($order),
        ]);
    }

    #[Route('/{id}/edit', methods: ['GET', 'POST'])]
    public function edit(
        Request $request,
        EmployeeRepository $employeeRepository,
        Order $order,
        DraftOrderService $draftOrderService,
    ): Response {
        $this->denyAccessUnlessGranted('DRAFT_ORDER_EDIT', $order);
        $employee = $employeeRepository->findOneBy(['directorate' => $order->getDirectorate(), 'user' => $this->getUser()]);
        $draft = $draftOrderService->makeDtoByOrder($order);
        $form = $this->createForm(DraftOrderType::class, $draft);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid() && null === $order->getNumber()) {
            $sign = null !== $request->get('btn_sign');
            $draftOrderService->applyDto($draft, $sign, $employee);

            if (null !== $order->getNumber()) {
                return $this->redirectToRoute('app_order_show', [
                    'directorateAlias' => $order->getDirectorate()->getAlias(),
                    'number' => $order->getNumber(),
                ], Response::HTTP_SEE_OTHER);
            }

            return $this->redirectToRoute('app_draftorder_index', [
                'directorateAlias' => $order->getDirectorate()->getAlias(),
            ], Response::HTTP_SEE_OTHER);
        }

        return $this->render('draft_order/edit.html.twig', [
            'order' => $order,
            'form' => $form,
        ]);
    }

    #[Route('/{id}', methods: ['POST'])]
    public function delete(Request $request, Order $order, EntityManagerInterface $entityManager): Response
    {
        $this->denyAccessUnlessGranted('DRAFT_ORDER_DELETE', $order);
        $directorateAlias = $order->getDirectorate()->getAlias();
        if ($this->isCsrfTokenValid('delete'.$order->getId(), $request->request->get('_token')) && null === $order->getNumber()) {
            $entityManager->remove($order);
            $entityManager->flush();
        }

        return $this->redirectToRoute('app_draftorder_index', ['directorateAlias' => $directorateAlias], Response::HTTP_SEE_OTHER);
    }
}
