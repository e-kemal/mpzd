<?php

declare(strict_types=1);

namespace App\Controller;

use App\Entity\User;
use App\Repository\UserRepository;
use App\Service\ForumService;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Exception\AccessDeniedHttpException;
use Symfony\Component\Routing\Annotation\Route;

class UserController extends AbstractController
{
    #[Route('/u/autocomplete')]
    public function autocomplete(
        Request $request,
        ForumService $forumService,
        UserRepository $userRepository,
        EntityManagerInterface $entityManager,
    ): Response {
        /** @var User $currentUser */
        $currentUser = $this->getUser();
        $f = false;
        if ($currentUser) {
            foreach ($currentUser->getEmployees() as $employee) {
                if ($employee->getPosition()->getAccessLevel() > 0) {
                    $f = true;
                    break;
                }
            }
        }
        if (!$f) {
            throw new AccessDeniedHttpException();
        }
        $results = [];
        $more = false;
        $q = $request->get('q', '');
        $page = $request->query->getInt('page', 1);
        if (!empty($q)) {
            $response = $forumService->findUsers($q, $page);
            $more = $response['totalPages'] > $response['page'];
            foreach ($response['results'] as $item) {
                $user = $userRepository->findOneBy(['externalId' => $item['id']]);
                if (!$user) {
                    $user = new User();
                    $user->setUsername($item['name'])->setExternalId($item['id']);
                    $entityManager->persist($user);
                    $entityManager->flush();
                }
                $results[] = [
                    'id' => $user->getId(),
                    'text' => $item['name'],
                ];
            }
        }

        return new JsonResponse([
            'results' => $results,
            'pagination' => ['more' => $more],
        ]);
    }
}
