<?php

declare(strict_types=1);

namespace App\Controller\Admin;

use App\Entity\HrOrder;
use App\Form\RankType;
use EasyCorp\Bundle\EasyAdminBundle\Config\Action;
use EasyCorp\Bundle\EasyAdminBundle\Config\Actions;
use EasyCorp\Bundle\EasyAdminBundle\Config\Crud;
use EasyCorp\Bundle\EasyAdminBundle\Controller\AbstractCrudController;
use EasyCorp\Bundle\EasyAdminBundle\Field\ArrayField;
use EasyCorp\Bundle\EasyAdminBundle\Field\AssociationField;
use EasyCorp\Bundle\EasyAdminBundle\Field\DateTimeField;
use EasyCorp\Bundle\EasyAdminBundle\Field\Field;
use EasyCorp\Bundle\EasyAdminBundle\Field\IntegerField;
use EasyCorp\Bundle\EasyAdminBundle\Field\NumberField;
use EasyCorp\Bundle\EasyAdminBundle\Field\TextEditorField;
use EasyCorp\Bundle\EasyAdminBundle\Field\TextField;

class HrOrderCrudController extends AbstractCrudController
{
    public static function getEntityFqcn(): string
    {
        return HrOrder::class;
    }

    public function configureCrud(Crud $crud): Crud
    {
        return $crud->setEntityLabelInPlural('HrOrders');
    }

    public function configureActions(Actions $actions): Actions
    {
        $actions->disable(Action::NEW);

        return $actions;
    }

    public function configureFields(string $pageName): iterable
    {
        yield AssociationField::new('directorate')->setRequired(true);
        yield IntegerField::new('year');
        yield NumberField::new('number');
        yield TextEditorField::new('text')->hideOnIndex();
        yield AssociationField::new('author')->autocomplete()->hideOnIndex();
        yield AssociationField::new('authorPosition')->hideOnIndex();
        yield IntegerField::new('authorRank')->setFormType(RankType::class)->hideOnIndex();
        yield DateTimeField::new('signedAt');
//        yield Field::new('draftData');
        yield ArrayField::new('employeeItems')->onlyOnDetail();
        yield IntegerField::new('externalId')->hideOnIndex();
        yield TextField::new('externalUrl')->hideOnIndex();
        yield DateTimeField::new('createdAt')->hideOnForm();
        yield DateTimeField::new('updatedAt')->hideOnForm();
    }
}
