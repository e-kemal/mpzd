<?php

declare(strict_types=1);

namespace App\Controller\Admin;

use App\Entity\OrderEmployeeItem;
use App\Form\RankType;
use EasyCorp\Bundle\EasyAdminBundle\Config\Action;
use EasyCorp\Bundle\EasyAdminBundle\Config\Actions;
use EasyCorp\Bundle\EasyAdminBundle\Config\Crud;
use EasyCorp\Bundle\EasyAdminBundle\Controller\AbstractCrudController;
use EasyCorp\Bundle\EasyAdminBundle\Field\AssociationField;
use EasyCorp\Bundle\EasyAdminBundle\Field\DateTimeField;
use EasyCorp\Bundle\EasyAdminBundle\Field\IntegerField;

class OrderEmployeeItemCrudController extends AbstractCrudController
{
    public static function getEntityFqcn(): string
    {
        return OrderEmployeeItem::class;
    }

    public function configureCrud(Crud $crud): Crud
    {
        return $crud->setEntityLabelInPlural('OrderEmployeeItems');
    }

    public function configureActions(Actions $actions): Actions
    {
        $actions->disable(Action::NEW);

        return $actions;
    }

    public function configureFields(string $pageName): iterable
    {
        yield AssociationField::new('order')->autocomplete()->hideWhenCreating();
        yield AssociationField::new('employee')->autocomplete()->hideWhenCreating();
        yield AssociationField::new('oldPosition')->hideOnIndex();
        yield AssociationField::new('newPosition')->hideOnIndex();
        yield IntegerField::new('oldRank')->setFormType(RankType::class)->hideOnIndex();
        yield IntegerField::new('newRank')->setFormType(RankType::class)->hideOnIndex();
        yield IntegerField::new('oldPoints')->hideOnIndex();
        yield IntegerField::new('newPoints')->hideOnIndex();
        yield DateTimeField::new('createdAt')->hideOnForm();
        yield DateTimeField::new('updatedAt')->hideOnForm();
    }
}
