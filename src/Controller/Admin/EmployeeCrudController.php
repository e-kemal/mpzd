<?php

declare(strict_types=1);

namespace App\Controller\Admin;

use App\Entity\Employee;
use App\Form\DataMapper\EmployeeDataMapper;
use App\Form\RankType;
use EasyCorp\Bundle\EasyAdminBundle\Config\Crud;
use EasyCorp\Bundle\EasyAdminBundle\Config\KeyValueStore;
use EasyCorp\Bundle\EasyAdminBundle\Context\AdminContext;
use EasyCorp\Bundle\EasyAdminBundle\Controller\AbstractCrudController;
use EasyCorp\Bundle\EasyAdminBundle\Dto\EntityDto;
use EasyCorp\Bundle\EasyAdminBundle\Field\ArrayField;
use EasyCorp\Bundle\EasyAdminBundle\Field\AssociationField;
use EasyCorp\Bundle\EasyAdminBundle\Field\DateTimeField;
use EasyCorp\Bundle\EasyAdminBundle\Field\IntegerField;
use Symfony\Component\Form\FormBuilderInterface;

class EmployeeCrudController extends AbstractCrudController
{
    public static function getEntityFqcn(): string
    {
        return Employee::class;
    }

    public function configureCrud(Crud $crud): Crud
    {
        return $crud->setEntityLabelInPlural('Employees');
    }

    public function configureFields(string $pageName): iterable
    {
        if (Crud::PAGE_EDIT !== $pageName) {
            yield AssociationField::new('user')->setRequired(true)->autocomplete();
            yield AssociationField::new('directorate');
        }
        yield IntegerField::new('number');
        yield AssociationField::new('position');
        yield IntegerField::new('rank')->setFormType(RankType::class);
        yield IntegerField::new('points');
        yield AssociationField::new('admittances')->onlyOnForms();
        yield ArrayField::new('admittances')->onlyOnDetail();
        if (Crud::PAGE_INDEX === $pageName || Crud::PAGE_DETAIL === $pageName) {
            yield DateTimeField::new('createdAt');
            yield DateTimeField::new('updatedAt');
        }
    }

    public function createEntity(string $entityFqcn)
    {
        return null;
    }

    public function createNewFormBuilder(EntityDto $entityDto, KeyValueStore $formOptions, AdminContext $context): FormBuilderInterface
    {
        $formBuilder = parent::createNewFormBuilder($entityDto, $formOptions, $context);
        $formBuilder->setDataMapper(new EmployeeDataMapper());
        $formBuilder->setEmptyData(null);

        return $formBuilder;
    }
}
