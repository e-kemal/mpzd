<?php

declare(strict_types=1);

namespace App\Controller\Admin;

use App\Entity\Position;
use App\Form\DataMapper\PositionDataMapper;
use EasyCorp\Bundle\EasyAdminBundle\Config\Crud;
use EasyCorp\Bundle\EasyAdminBundle\Config\Filters;
use EasyCorp\Bundle\EasyAdminBundle\Config\KeyValueStore;
use EasyCorp\Bundle\EasyAdminBundle\Context\AdminContext;
use EasyCorp\Bundle\EasyAdminBundle\Controller\AbstractCrudController;
use EasyCorp\Bundle\EasyAdminBundle\Dto\EntityDto;
use EasyCorp\Bundle\EasyAdminBundle\Field\AssociationField;
use EasyCorp\Bundle\EasyAdminBundle\Field\DateTimeField;
use EasyCorp\Bundle\EasyAdminBundle\Field\IntegerField;
use EasyCorp\Bundle\EasyAdminBundle\Field\TextField;
use Symfony\Component\Form\FormBuilderInterface;

class PositionCrudController extends AbstractCrudController
{
    public static function getEntityFqcn(): string
    {
        return Position::class;
    }

    public function configureCrud(Crud $crud): Crud
    {
        return $crud->setEntityLabelInPlural('Positions');
    }

    public function configureFields(string $pageName): iterable
    {
        yield TextField::new('name');
        yield TextField::new('shortName');
        if (Crud::PAGE_EDIT !== $pageName) {
            yield AssociationField::new('directorate')->setRequired(true);
        }
        yield IntegerField::new('accessLevel');
        if (Crud::PAGE_INDEX !== $pageName) {
            yield TextField::new('shoulderMarkFileName');
        }
        if (Crud::PAGE_INDEX === $pageName || Crud::PAGE_DETAIL === $pageName) {
            yield DateTimeField::new('createdAt');
            yield DateTimeField::new('updatedAt');
        }
    }

    public function configureFilters(Filters $filters): Filters
    {
        $filters->add('directorate');

        return $filters;
    }

    public function createEntity(string $entityFqcn)
    {
        return null;
    }

    public function createNewFormBuilder(EntityDto $entityDto, KeyValueStore $formOptions, AdminContext $context): FormBuilderInterface
    {
        $formBuilder = parent::createNewFormBuilder($entityDto, $formOptions, $context);
        $formBuilder->setDataMapper(new PositionDataMapper());
        $formBuilder->setEmptyData(null);

        return $formBuilder;
    }
}
