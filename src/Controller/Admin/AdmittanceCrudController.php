<?php

declare(strict_types=1);

namespace App\Controller\Admin;

use App\Entity\Admittance;
use App\Form\DataMapper\AdmittanceDataMapper;
use EasyCorp\Bundle\EasyAdminBundle\Config\Crud;
use EasyCorp\Bundle\EasyAdminBundle\Config\Filters;
use EasyCorp\Bundle\EasyAdminBundle\Config\KeyValueStore;
use EasyCorp\Bundle\EasyAdminBundle\Context\AdminContext;
use EasyCorp\Bundle\EasyAdminBundle\Controller\AbstractCrudController;
use EasyCorp\Bundle\EasyAdminBundle\Dto\EntityDto;
use EasyCorp\Bundle\EasyAdminBundle\Field\AssociationField;
use EasyCorp\Bundle\EasyAdminBundle\Field\DateTimeField;
use EasyCorp\Bundle\EasyAdminBundle\Field\TextField;
use Symfony\Component\Form\FormBuilderInterface;

class AdmittanceCrudController extends AbstractCrudController
{
    public static function getEntityFqcn(): string
    {
        return Admittance::class;
    }

    public function configureCrud(Crud $crud): Crud
    {
        return $crud->setEntityLabelInPlural('Admittances');
    }

    public function configureFields(string $pageName): iterable
    {
        yield TextField::new('name');
        if (Crud::PAGE_EDIT !== $pageName) {
            yield AssociationField::new('directorate')->setRequired(true);
        }
        yield DateTimeField::new('createdAt')->hideOnForm();
        yield DateTimeField::new('updatedAt')->hideOnForm();
    }

    public function configureFilters(Filters $filters): Filters
    {
        $filters->add('directorate');

        return $filters;
    }

    public function createEntity(string $entityFqcn)
    {
        return null;
    }

    public function createNewFormBuilder(EntityDto $entityDto, KeyValueStore $formOptions, AdminContext $context): FormBuilderInterface
    {
        $formBuilder = parent::createNewFormBuilder($entityDto, $formOptions, $context);
        $formBuilder->setDataMapper(new AdmittanceDataMapper());
        $formBuilder->setEmptyData(null);

        return $formBuilder;
    }
}
