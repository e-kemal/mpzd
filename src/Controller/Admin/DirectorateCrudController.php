<?php

declare(strict_types=1);

namespace App\Controller\Admin;

use App\Entity\Directorate;
use EasyCorp\Bundle\EasyAdminBundle\Config\Crud;
use EasyCorp\Bundle\EasyAdminBundle\Controller\AbstractCrudController;
use EasyCorp\Bundle\EasyAdminBundle\Field\ArrayField;
use EasyCorp\Bundle\EasyAdminBundle\Field\BooleanField;
use EasyCorp\Bundle\EasyAdminBundle\Field\DateTimeField;
use EasyCorp\Bundle\EasyAdminBundle\Field\IntegerField;
use EasyCorp\Bundle\EasyAdminBundle\Field\TextField;

class DirectorateCrudController extends AbstractCrudController
{
    public static function getEntityFqcn(): string
    {
        return Directorate::class;
    }

    public function configureCrud(Crud $crud): Crud
    {
        return $crud->setEntityLabelInPlural('Directorates');
    }

    public function configureFields(string $pageName): iterable
    {
        yield TextField::new('name');
        yield TextField::new('shortName');
        yield TextField::new('alias');
        yield BooleanField::new('root')->renderAsSwitch(false);
        yield IntegerField::new('ordersTopicId');
        yield IntegerField::new('hrOrdersTopicId');
        if (Crud::PAGE_INDEX !== $pageName) {
            yield IntegerField::new('employeesNumberFieldId');
            yield IntegerField::new('employeesShoulderMarkFieldId');
            yield IntegerField::new('employeesPointsFieldId');
            yield IntegerField::new('employeesAdmittancesFieldId');
        }
        if (Crud::PAGE_DETAIL === $pageName) {
            yield ArrayField::new('positions');
        }
        if (Crud::PAGE_INDEX === $pageName || Crud::PAGE_DETAIL === $pageName) {
            yield DateTimeField::new('createdAt');
            yield DateTimeField::new('updatedAt');
        }
    }
}
