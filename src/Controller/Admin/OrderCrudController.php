<?php

declare(strict_types=1);

namespace App\Controller\Admin;

use App\Entity\Order;
use App\Form\RankType;
use EasyCorp\Bundle\EasyAdminBundle\Config\Action;
use EasyCorp\Bundle\EasyAdminBundle\Config\Actions;
use EasyCorp\Bundle\EasyAdminBundle\Config\Crud;
use EasyCorp\Bundle\EasyAdminBundle\Controller\AbstractCrudController;
use EasyCorp\Bundle\EasyAdminBundle\Field\ArrayField;
use EasyCorp\Bundle\EasyAdminBundle\Field\AssociationField;
use EasyCorp\Bundle\EasyAdminBundle\Field\DateTimeField;
use EasyCorp\Bundle\EasyAdminBundle\Field\Field;
use EasyCorp\Bundle\EasyAdminBundle\Field\IntegerField;
use EasyCorp\Bundle\EasyAdminBundle\Field\NumberField;
use EasyCorp\Bundle\EasyAdminBundle\Field\TextEditorField;
use EasyCorp\Bundle\EasyAdminBundle\Field\TextField;

class OrderCrudController extends AbstractCrudController
{
    public static function getEntityFqcn(): string
    {
        return Order::class;
    }

    public function configureCrud(Crud $crud): Crud
    {
        return $crud->setEntityLabelInPlural('Orders');
    }

    public function configureActions(Actions $actions): Actions
    {
        $actions->disable(Action::NEW);

        return $actions;
    }

    public function configureFields(string $pageName): iterable
    {
        yield AssociationField::new('directorate')->setRequired(true);
        yield NumberField::new('number');
        yield TextField::new('title');
        yield TextEditorField::new('text')->hideOnIndex();
        yield AssociationField::new('author')->autocomplete()->hideOnIndex();
        yield AssociationField::new('authorPosition')->hideOnIndex();
        yield IntegerField::new('authorRank')->setFormType(RankType::class)->hideOnIndex();
        yield DateTimeField::new('signedAt');
        yield ArrayField::new('cancelOrders')->onlyOnDetail();
        yield AssociationField::new('canceledBy')->autocomplete();
//        yield Field::new('draftData');
        yield IntegerField::new('externalId')->hideOnIndex();
        yield TextField::new('externalUrl')->hideOnIndex();
        yield DateTimeField::new('createdAt')->hideOnForm();
        yield DateTimeField::new('updatedAt')->hideOnForm();
    }
}
