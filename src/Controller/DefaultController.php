<?php

declare(strict_types=1);

namespace App\Controller;

use App\Repository\DirectorateRepository;
use App\Repository\OrderRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class DefaultController extends AbstractController
{
    #[Route('/')]
    public function index(DirectorateRepository $directorateRepository, OrderRepository $orderRepository): Response
    {
        $directorates = $directorateRepository->findAll();
        $orders = $orderRepository->findLatestByDirectorate();

        return $this->render('default/index.html.twig', [
            'directorates' => $directorates,
            'orders' => $orders,
        ]);
    }
}
