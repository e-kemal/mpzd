<?php

declare(strict_types=1);

namespace App\Command;

use App\Message\UpdateEmployeeFields;
use App\Repository\DirectorateRepository;
use App\Repository\EmployeeRepository;
use Symfony\Component\Console\Attribute\AsCommand;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Style\SymfonyStyle;
use Symfony\Component\Messenger\MessageBusInterface;

#[AsCommand(
    name: 'app:employee:update-fields',
    description: 'Обновляет поля на форуме для заданного работника',
)]
class EmployeeUpdateFieldsCommand extends Command
{
    public function __construct(
        private DirectorateRepository $directorateRepository,
        private EmployeeRepository $employeeRepository,
        private MessageBusInterface $messageBus,
    ) {
        parent::__construct();
    }

    protected function configure(): void
    {
        $this
            ->addArgument('directorate-alias', InputArgument::REQUIRED, 'Алиас дирекции')
            ->addArgument('number', InputArgument::REQUIRED, 'Табельный номер работника')
        ;
    }

    protected function execute(InputInterface $input, OutputInterface $output): int
    {
        $io = new SymfonyStyle($input, $output);

        $directorate = $this->directorateRepository->findOneBy(['alias' => $input->getArgument('directorate-alias')]);
        if (!$directorate) {
            $io->error(sprintf('Дирекция с алиасом "%s" не найдена', $input->getArgument('directorate-alias')));

            return Command::FAILURE;
        }

        $employee = $this->employeeRepository->findOneBy(['directorate' => $directorate, 'number' => $input->getArgument('number')]);
        if (!$employee) {
            $io->error(sprintf('Работник с табельным номером %s не найден в дирекции "%s"', $input->getArgument('number'), $directorate));

            return Command::FAILURE;
        }

        $this->messageBus->dispatch(new UpdateEmployeeFields($employee->getId()));
        $io->success(sprintf('Команда обновления полей работника "%s" выполнена', $employee));

        return Command::SUCCESS;
    }
}
