<?php

declare(strict_types=1);

namespace App\Service;

use GuzzleHttp\Client;

class ForumService
{
    private const POST_ENDPOINT = 'forums/posts';
    private const USERS_ENDPOINT = 'core/members';
    private const USER_ENDPOINT = 'core/members/%s';

    public function __construct(
        private string $key,
        private string $url,
    ) {
    }

    public function post(int $topicId, int $userId, string $text): array
    {
        $client = new Client([
//            'base_url' => $this->url,
            'auth' => [$this->key, ''],
        ]);
        $response = $client->post($this->url.self::POST_ENDPOINT, ['form_params' => [
            'topic' => $topicId,
            'author' => $userId,
            'post' => $text,
        ]]);

        return json_decode($response->getBody()->getContents(), true);
    }

    public function findUsers(string $q, int $page = 1): array
    {
        $client = new Client([
            'auth' => [$this->key, ''],
        ]);
        $response = $client->get($this->url.self::USERS_ENDPOINT, ['query' => [
            'name' => $q,
            'page' => $page,
        ]]);

        return json_decode($response->getBody()->getContents(), true);
    }

    public function updateCustomFields(int $id, array $fields): array
    {
        $client = new Client([
            'auth' => [$this->key, ''],
        ]);
        $response = $client->post($this->url.sprintf(self::USER_ENDPOINT, $id), ['form_params' => [
            'customFields' => $fields,
        ]]);

        return json_decode($response->getBody()->getContents(), true);
    }
}
