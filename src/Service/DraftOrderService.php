<?php

declare(strict_types=1);

namespace App\Service;

use App\Dto\DraftOrder;
use App\Entity\Employee;
use App\Entity\Order;
use App\Message\PostOrder;
use App\Repository\OrderRepository;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\Messenger\MessageBusInterface;

class DraftOrderService
{
    public function __construct(
        private OrderRepository $orderRepository,
        private EntityManagerInterface $entityManager,
        private MessageBusInterface $messageBus,
    ) {
    }

    public function makeNewDto(Employee $employee): DraftOrder
    {
        $order = new Order($employee);
        $this->entityManager->persist($order);

        return $this->makeDtoByOrder($order);
    }

    public function makeDtoByOrder(Order $order): DraftOrder
    {
        $draft = new DraftOrder($order);
        $draft->title = $order->getTitle();
        $draft->text = $order->getText();
        $draftData = $order->getDraftData();
        if (array_key_exists('cancelOrders', $draftData)) {
            foreach ($draftData['cancelOrders'] as $cancelOrderId) {
                if ($cancelOrder = $this->orderRepository->find($cancelOrderId)) {
                    $draft->cancelOrders[] = $cancelOrder;
                }
            }
        }

        return $draft;
    }

    public function applyDto(DraftOrder $draftOrder, bool $sign = false, ?Employee $employee = null): void
    {
        $order = $draftOrder->getOrder();
        $order
            ->setTitle($draftOrder->title)
            ->setText($draftOrder->text)
        ;
        $draftData = [];
        foreach ($draftOrder->cancelOrders as $cancelOrder) {
            $draftData['cancelOrders'][] = $cancelOrder->getId();
        }
        $order->setDraftData($draftData);
        if ($sign) {
            foreach ($draftOrder->cancelOrders as $cancelOrder) {
                $order->addCancelOrder($cancelOrder);
            }
            $number = $this->orderRepository->findMaxUsedNumberByOrder($order);
            if (null === $number) {
                $number = 1;
            } else {
                ++$number;
            }
            $order->setNumber($number);
            $order->setSignedAt(new \DateTimeImmutable());
            if (null !== $employee) {
                $order
                    ->setAuthor($employee)
                    ->setAuthorPosition($employee->getPosition())
                    ->setAuthorRank($employee->getRank())
                ;
            }
        }
        $this->entityManager->flush();
        if ($sign) {
            $this->messageBus->dispatch(new PostOrder($order->getId()));
        }
    }
}
