<?php

declare(strict_types=1);

namespace App\Service;

use App\Dto\DraftHrOrder;
use App\Dto\DraftHrOrderItem;
use App\Entity\Admittance;
use App\Entity\Employee;
use App\Entity\HrOrder;
use App\Message\PostHrOrder;
use App\Message\UpdateEmployeeFields;
use App\Repository\AdmittanceRepository;
use App\Repository\DirectorateRepository;
use App\Repository\EmployeeRepository;
use App\Repository\HrOrderRepository;
use App\Repository\PositionRepository;
use App\Repository\UserRepository;
use App\Type\Rank;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\Messenger\MessageBusInterface;

class DraftHrOrderService
{
    public function __construct(
        private DirectorateRepository $directorateRepository,
        private UserRepository $userRepository,
        private EmployeeRepository $employeeRepository,
        private PositionRepository $positionRepository,
        private HrOrderRepository $orderRepository,
        private AdmittanceRepository $admittanceRepository,
        private EntityManagerInterface $entityManager,
        private MessageBusInterface $messageBus,
    ) {
    }

    public function makeNewDto(Employee $employee): DraftHrOrder
    {
        $order = new HrOrder($employee);
        $this->entityManager->persist($order);

        return $this->makeDtoByOrder($order);
    }

    public function makeDtoByOrder(HrOrder $order): DraftHrOrder
    {
        $draft = new DraftHrOrder($order);
        $draft->text = $order->getText();
        $draftData = $order->getDraftData();
        if (array_key_exists('directorate', $draftData) && $d = $this->directorateRepository->find($draftData['directorate'])) {
            $draft->directorate = $d;
        } else {
            $draft->directorate = $order->getDirectorate();
        }
        if (array_key_exists('items', $draftData)) {
            foreach ($draftData['items'] as $item) {
                if (array_key_exists('user', $item) && $user = $this->userRepository->find($item['user'])) {
                    $orderItem = new DraftHrOrderItem();
                    $orderItem->user = $user;
                    $orderItem->employee = $this->employeeRepository->findOneBy(['directorate' => $draft->directorate, 'user' => $user]);
                    $orderItem->changePosition = array_key_exists('position', $item);
                    $orderItem->position = ($item['position'] ?? null) ? $this->positionRepository->find($item['position']) : null;
                    $orderItem->changeRank = array_key_exists('rank', $item);
                    $orderItem->rank = new Rank($item['rank'] ?? null);
                    $orderItem->setPoints = $item['setPoints'] ?? false;
                    $orderItem->points = $item['points'] ?? 0;
                    $orderItem->addAdmittances = empty($item['addAdmittances']) ? [] : $this->admittanceRepository->findBy(['id' => $item['addAdmittances']]);
                    $orderItem->removeAdmittances = empty($item['removeAdmittances']) ? [] : $this->admittanceRepository->findBy(['id' => $item['removeAdmittances']]);
                    $draft->items[] = $orderItem;
                }
            }
        }

        return $draft;
    }

    public function applyDto(DraftHrOrder $draft, bool $sign = false, ?Employee $employee = null): void
    {
        $order = $draft->getOrder();
        $order->setText($draft->text);
        $draftData = [];
        $draftData['directorate'] = $draft->directorate->getId();
        foreach ($draft->items as $item) {
            if (!$item->user) {
                continue;
            }
            $data = [
                'user' => $item->user->getId(),
            ];
            if ($item->changePosition) {
                $data['position'] = $item->position?->getId();
            }
            if ($item->changeRank) {
                $data['rank'] = $item->rank->getValue();
            }
            $data['setPoints'] = $item->setPoints;
            $data['points'] = $item->points;
            $data['addAdmittances'] = array_map(fn (Admittance $a) => $a->getId(), is_array($item->addAdmittances) ? $item->addAdmittances : iterator_to_array($item->addAdmittances));
            $data['removeAdmittances'] = array_map(fn (Admittance $a) => $a->getId(), is_array($item->removeAdmittances) ? $item->removeAdmittances : iterator_to_array($item->removeAdmittances));
            $draftData['items'][] = $data;
        }
        $order->setDraftData($draftData);
        if ($sign) {
            $number = $this->employeeRepository->findMaxUsedNumberByDirectorate($draft->directorate);
            if (null === $number) {
                $number = 0;
            }
            foreach ($draft->items as $item) {
                if (!$item->user) {
                    continue;
                }

                $item->employee = $this->employeeRepository->findOneBy(['directorate' => $draft->directorate, 'user' => $item->user]);
                if (null === $item->employee) {
                    $item->employee = new Employee($item->user, $draft->directorate);
                    $this->entityManager->persist($item->employee);
                    $item->employee->setNumber(++$number);
                }
                $orderItem = $order->createEmployeeItem($item->employee);
                $this->entityManager->persist($orderItem);
                $orderItem
                    ->setOldPosition($item->employee->getPosition())
                    ->setOldRank($item->employee->getRank())
                    ->setOldPoints($item->employee->getPoints())
                ;
                if ($item->changePosition) {
                    $item->employee->setPosition($item->position);
                }
                if ($item->changeRank) {
                    $item->employee->setRank($item->rank);
                }
                if ($item->setPoints) {
                    $item->employee->setPoints($item->points);
                } else {
                    $item->employee->setPoints($item->employee->getPoints() + $item->points);
                }
                foreach ($item->addAdmittances as $admittance) {
                    $item->employee->addAdmittance($admittance);
                }
                foreach ($item->removeAdmittances as $admittance) {
                    $item->employee->removeAdmittance($admittance);
                }
                $orderItem
                    ->setNewPosition($item->employee->getPosition())
                    ->setNewRank($item->employee->getRank())
                    ->setNewPoints($item->employee->getPoints())
                    ->setAddAdmittanceIds(array_map(fn (Admittance $a) => $a->getId(), is_array($item->addAdmittances) ? $item->addAdmittances : iterator_to_array($item->addAdmittances)))
                    ->setRemoveAdmittanceIds(array_map(fn (Admittance $a) => $a->getId(), is_array($item->removeAdmittances) ? $item->removeAdmittances : iterator_to_array($item->removeAdmittances)))
                    ->setActualAdmittanceIds(array_map(fn (Admittance $a) => $a->getId(), $item->employee->getAdmittances()))
                ;
            }

            $order->setSignedAt(new \DateTimeImmutable());
            $order->setYear((int) $order->getSignedAt()->format('Y'));
            $number = $this->orderRepository->findMaxUsedNumberByOrder($order);
            if (null === $number) {
                $number = 1;
            } else {
                ++$number;
            }
            $order->setNumber($number);
            if (null !== $employee) {
                $order
                    ->setAuthor($employee)
                    ->setAuthorPosition($employee->getPosition())
                    ->setAuthorRank($employee->getRank())
                ;
            }
        }
        $this->entityManager->flush();
        if ($sign) {
            $this->messageBus->dispatch(new PostHrOrder($order->getId()));
            foreach ($draft->items as $item) {
                $this->messageBus->dispatch(new UpdateEmployeeFields($item->employee->getId()));
            }
        }
    }
}
