<?php

declare(strict_types=1);

namespace App\Dto;

use App\Entity\Directorate;
use App\Entity\HrOrder;
use Symfony\Component\Validator\Constraints as Assert;
use Symfony\Component\Validator\Context\ExecutionContextInterface;

class DraftHrOrder
{
    public function __construct(
        private HrOrder $order,
    ) {
    }

    public function getOrder(): HrOrder
    {
        return $this->order;
    }

    public Directorate $directorate;

    #[Assert\NotBlank]
    public string $text = '';

    /** @var DraftHrOrderItem[] */
    public array $items = [];

    #[Assert\Callback]
    public function validateDirectorate(ExecutionContextInterface $context)
    {
        if (!$this->order->getDirectorate()->isRoot() && !$this->directorate->getId()->equals($this->order->getDirectorate()->getId())) {
            $context
                ->buildViolation('hr_order.directorate.must_be_equal_to_order_directorate')
                ->atPath('directorate')
                ->addViolation()
            ;
        }
        foreach ($this->items as $index => $orderItem) {
            if ($orderItem->employee && !$orderItem->employee->getDirectorate()->getId()->equals($this->directorate->getId())) {
                $context
                    ->buildViolation('hr_order.items_employee.must_belong_selected_directorate')
                    ->atPath(sprintf('items[%s].employee', $index))
                    ->addViolation()
                ;
            }
            if ($orderItem->position && !$orderItem->position->getDirectorate()->getId()->equals($this->directorate->getId())) {
                $context
                    ->buildViolation('hr_order.items_position.must_belong_selected_directorate')
                    ->atPath(sprintf('items[%s].position', $index))
                    ->addViolation()
                ;
            }
            foreach ($orderItem->addAdmittances as $admittance) {
                if (!$admittance->getDirectorate()->getId()->equals($this->directorate->getId())) {
                    $context
                        ->buildViolation('hr_order.items_add_admittances.must_belong_selected_directorate')
                        ->atPath(sprintf('items[%s].addAdmittances', $index))
                        ->addViolation()
                    ;
                }
                if ($orderItem->employee && $orderItem->employee->containsAdmittance($admittance)) {
                    $context
                        ->buildViolation('hr_order.items_add_admittances.admittances_already_contains_in_employee')
                        ->atPath(sprintf('items[%s].addAdmittances', $index))
                        ->addViolation()
                    ;
                }
            }
            if ($orderItem->employee) {
                foreach ($orderItem->removeAdmittances as $admittance) {
                    if (!$orderItem->employee->containsAdmittance($admittance)) {
                        $context
                            ->buildViolation('hr_order.items_remove_admittances.must_contains_in_employee_admittances')
                            ->atPath(sprintf('items[%s].removeAdmittances', $index))
                            ->addViolation()
                        ;
                    }
                }
            }
        }
    }
}
