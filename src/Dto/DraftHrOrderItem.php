<?php

declare(strict_types=1);

namespace App\Dto;

use App\Entity\Admittance;
use App\Entity\Employee;
use App\Entity\Position;
use App\Entity\User;
use App\Type\Rank;
use Doctrine\Common\Collections\Collection;
use Symfony\Component\Validator\Constraints as Assert;

class DraftHrOrderItem
{
    #[Assert\NotBlank]
    public ?User $user = null;
    public ?Employee $employee = null;
    public bool $changePosition = false;
    public ?Position $position = null;
    public bool $changeRank = false;
    public Rank $rank;
    public bool $setPoints = false;
    public int $points = 0;
    /** @var Admittance[] */
    public array|Collection $addAdmittances = [];
    /** @var Admittance[] */
    public array|Collection $removeAdmittances = [];

    public function __construct()
    {
        $this->rank = new Rank(null);
    }
}
