<?php

declare(strict_types=1);

namespace App\Dto;

use App\Entity\Order;
use Symfony\Component\Validator\Constraints as Assert;
use Symfony\Component\Validator\Context\ExecutionContextInterface;

class DraftOrder
{
    public function __construct(
        private Order $order,
    ) {
    }

    public function getOrder(): Order
    {
        return $this->order;
    }

    public function getDirectorateAlias(): string
    {
        return $this->order->getDirectorate()->getAlias();
    }

    #[Assert\NotBlank]
    public string $title;

    #[Assert\NotBlank]
    public string $text;

    /** @var Order[] */
    #[Assert\Unique]
    public array $cancelOrders = [];

    #[Assert\Callback]
    public function validateCancelOrders(ExecutionContextInterface $context)
    {
        foreach ($this->cancelOrders as $index => $cancelOrder) {
            if (
                !$this->order->getDirectorate()->isRoot()
                && !$this->order->getDirectorate()->getId()->equals($cancelOrder->getId())
            ) {
                $context
                    ->buildViolation('order.cancel_orders.belongs_to_another_directorate')
                    ->atPath(sprintf('cancelOrders[%s]', $index))
                    ->addViolation()
                ;
            }
            if (null === $cancelOrder->getNumber()) {
                $context
                    ->buildViolation('order.cancel_orders.cannot_cancel_draft')
                    ->atPath(sprintf('cancelOrders[%s]', $index))
                    ->addViolation()
                ;
            }
            if (null !== $cancelOrder->getCanceledBy()) {
                $context
                    ->buildViolation('order.cancel_orders.cannot_cancel_already_canceled')
                    ->atPath(sprintf('cancelOrders[%s]', $index))
                    ->addViolation()
                ;
            }
        }
    }
}
