<?php

declare(strict_types=1);

namespace App\Security\Voter;

use App\Entity\Directorate;
use App\Entity\HrOrder;
use App\Entity\Order;
use App\Entity\User;
use Symfony\Bundle\SecurityBundle\Security;
use Symfony\Component\Security\Core\Authentication\Token\TokenInterface;
use Symfony\Component\Security\Core\Authorization\Voter\Voter;

class DraftOrderVoter extends Voter
{
    public const CREATE = 'DRAFT_ORDER_CREATE';
    public const DELETE = 'DRAFT_ORDER_DELETE';
    public const EDIT = 'DRAFT_ORDER_EDIT';
    public const LIST = 'DRAFT_ORDER_LIST';
    public const SHOW = 'DRAFT_ORDER_SHOW';

    public function __construct(
        private Security $security,
    ) {
    }

    protected function supports(string $attribute, $subject): bool
    {
        if (!in_array($attribute, [self::CREATE, self::DELETE, self::EDIT, self::LIST, self::SHOW])) {
            return false;
        }
        if (!$subject instanceof Order && !$subject instanceof HrOrder && !$subject instanceof Directorate) {
            return false;
        }

        return true;
    }

    protected function voteOnAttribute(string $attribute, $subject, TokenInterface $token): bool
    {
        if (self::CREATE != $attribute && $this->security->isGranted('ROLE_ADMIN')) {
            return true;
        }

        if ($subject instanceof Order) {
            $subject = $subject->getDirectorate();
        }
        if ($subject instanceof HrOrder) {
            $subject = $subject->getDirectorate();
        }
        if (!$subject instanceof Directorate) {
            return false;
        }
        $user = $token->getUser();
        // if the user is anonymous, do not grant access
        if (!$user instanceof User) {
            return false;
        }
        $employee = $user->getEmployees()[(string) $subject->getId()] ?? null;

        return $employee && $employee->getPosition()->getAccessLevel() > 0;
    }
}
