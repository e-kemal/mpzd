<?php

declare(strict_types=1);

namespace App\Type;

class Rank
{
    public function __construct(
        private ?int $value,
    ) {
    }

    public function __toString(): string
    {
        if (null === $this->value) {
            return 'без класса';
        }
        if (0 === $this->value) {
            return 'внеклассный';
        }

        return $this->value.' класс';
    }

    public function getValue(): ?int
    {
        return $this->value;
    }
}
