<?php

declare(strict_types=1);

namespace App\Type;

use Doctrine\DBAL\Platforms\AbstractPlatform;
use Doctrine\DBAL\Types\IntegerType;

class RankType extends IntegerType
{
    public const RANK = 'rank';

    /**
     * @param Rank $value
     */
    public function convertToDatabaseValue($value, AbstractPlatform $platform): ?int
    {
        return $value->getValue();
    }

    public function convertToPHPValue($value, AbstractPlatform $platform): Rank
    {
        return new Rank(null === $value ? null : (int) $value);
    }

    public function getName(): string
    {
        return self::RANK;
    }

    public function requiresSQLCommentHint(AbstractPlatform $platform): bool
    {
        return true;
    }
}
