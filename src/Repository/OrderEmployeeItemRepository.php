<?php

declare(strict_types=1);

namespace App\Repository;

use App\Entity\Admittance;
use App\Entity\Employee;
use App\Entity\HrOrder;
use App\Entity\OrderEmployeeItem;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method OrderEmployeeItem|null find($id, $lockMode = null, $lockVersion = null)
 * @method OrderEmployeeItem|null findOneBy(array $criteria, array $orderBy = null)
 * @method OrderEmployeeItem[]    findAll()
 * @method OrderEmployeeItem[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class OrderEmployeeItemRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, OrderEmployeeItem::class);
    }

    /**
     * @return OrderEmployeeItem[]
     */
    public function findItemsByEmployee(Employee $employee): array
    {
        $items = $this->createQueryBuilder('i')
            ->join('i.order', 'o')
            ->leftJoin('i.oldPosition', 'op')
            ->leftJoin('i.newPosition', 'np')
            ->addSelect('o', 'op', 'np')
            ->where('i.employee = :employee')
            ->setParameter('employee', $employee)
            ->orderBy('o.signedAt')
            ->getQuery()
            ->getResult()
        ;
        $this->appendAdmittancesToItems($items);

        return $items;
    }

    /**
     * @return OrderEmployeeItem[]
     */
    public function findItemsByOrder(HrOrder $order): array
    {
        $items = $this->createQueryBuilder('i')
            ->join('i.employee', 'e')
            ->join('e.directorate', 'ed')
            ->join('e.user', 'eu')
            ->leftJoin('i.oldPosition', 'op')
            ->leftJoin('i.newPosition', 'np')
            ->addSelect('e', 'ed', 'eu', 'op', 'np')
            ->where('i.order = :order')
            ->setParameter('order', $order)
            ->orderBy('e.number')
            ->getQuery()
            ->getResult()
        ;
        $this->appendAdmittancesToItems($items);

        return $items;
    }

    /**
     * @param OrderEmployeeItem[] $items
     */
    private function appendAdmittancesToItems(array &$items): void
    {
        $admittanceRepository = $this->_em->getRepository(Admittance::class);
        foreach ($items as $item) {
            $ids = $item->getAddAdmittanceIds();
            $item->setAddAdmittances(empty($ids) ? [] : $admittanceRepository->findBy(['id' => $ids]));
            $ids = $item->getRemoveAdmittanceIds();
            $item->setRemoveAdmittances(empty($ids) ? [] : $admittanceRepository->findBy(['id' => $ids]));
        }
    }
}
