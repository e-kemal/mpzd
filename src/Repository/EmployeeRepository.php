<?php

declare(strict_types=1);

namespace App\Repository;

use App\Entity\Directorate;
use App\Entity\Employee;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;
use Knp\Component\Pager\Pagination\PaginationInterface;
use Knp\Component\Pager\PaginatorInterface;

/**
 * @method Employee|null find($id, $lockMode = null, $lockVersion = null)
 * @method Employee|null findOneBy(array $criteria, array $orderBy = null)
 * @method Employee[]    findAll()
 * @method Employee[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class EmployeeRepository extends ServiceEntityRepository
{
    public function __construct(
        ManagerRegistry $registry,
        private PaginatorInterface $paginator
    ) {
        parent::__construct($registry, Employee::class);
    }

    /**
     * @return PaginationInterface|Employee[]
     */
    public function paginateByDirectorate(Directorate $directorate, ?int $page): PaginationInterface|array
    {
        $qb = $this->createQueryBuilder('e')
            ->addSelect('position')
            ->addSelect('user')
            ->leftJoin('e.position', 'position')
            ->leftJoin('e.user', 'user')
            ->where('e.directorate = :directorate')
            ->setParameter('directorate', $directorate)
            ->orderBy('e.number')
        ;
        if (null === $page) {
            return $qb->getQuery()->getResult();
        }

        return $this->paginator->paginate($qb, $page);
    }

    public function findMaxUsedNumberByDirectorate(Directorate $directorate): ?int
    {
        return $this->createQueryBuilder('e')
            ->select('MAX(e.number)')
            ->where('e.directorate = :directorate')
            ->setParameter('directorate', $directorate)
            ->getQuery()
            ->getSingleScalarResult()
        ;
    }

    /**
     * @return Employee[]
     */
    public function findLeadersByDirectorate(Directorate $directorate): array
    {
        return $this->createQueryBuilder('e')
            ->addSelect('position')
            ->addSelect('user')
            ->join('e.position', 'position')
            ->join('e.user', 'user')
            ->where('e.directorate = :directorate')
            ->setParameter('directorate', $directorate)
            ->andWhere('position.accessLevel > 0')
            ->orderBy('position.accessLevel', 'DESC')
            ->addOrderBy('e.number')
            ->getQuery()
            ->getResult()
        ;
    }
}
