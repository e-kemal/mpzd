<?php

declare(strict_types=1);

namespace App\Repository;

use App\Entity\Directorate;
use App\Entity\Order;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;
use Knp\Component\Pager\Pagination\PaginationInterface;
use Knp\Component\Pager\PaginatorInterface;

/**
 * @method Order|null find($id, $lockMode = null, $lockVersion = null)
 * @method Order|null findOneBy(array $criteria, array $orderBy = null)
 * @method Order[]    findAll()
 * @method Order[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class OrderRepository extends ServiceEntityRepository
{
    public function __construct(
        ManagerRegistry $registry,
        private PaginatorInterface $paginator,
    ) {
        parent::__construct($registry, Order::class);
    }

    /**
     * @return PaginationInterface|Order[]
     */
    public function paginateByDirectorate(Directorate $directorate, bool $signed = true, ?int $page = null): PaginationInterface|array
    {
        $qb = $this->createQueryBuilder('o')
            ->addSelect('a')
            ->addSelect('ap')
            ->addSelect('u')
            ->leftJoin('o.author', 'a')
            ->leftJoin('o.authorPosition', 'ap')
            ->leftJoin('a.user', 'u')
            ->where('o.directorate = :directorate')
            ->andWhere($signed ? 'o.number IS NOT NULL' : 'o.number IS NULL')
            ->setParameter('directorate', $directorate)
            ->orderBy($signed ? 'o.number' : 'o.createdAt', 'DESC')
        ;
        if (null === $page) {
            return $qb->getQuery()->getResult();
        }

        return $this->paginator->paginate($qb, $page);
    }

    public function findMaxUsedNumberByOrder(Order $order): ?int
    {
        return $this->createQueryBuilder('o')
            ->select('MAX(o.number)')
            ->where('o.directorate = :directorate')
            ->setParameter('directorate', $order->getDirectorate())
            ->getQuery()
            ->getSingleScalarResult()
        ;
    }

    /**
     * @return Order[]
     */
    public function findByDirectorateAndQuery(?Directorate $directorate, string $query, int $offset, int $limit): array
    {
        $qb = $this->createQueryBuilder('o')
            ->addSelect('ap')
            ->leftJoin('o.authorPosition', 'ap')
            ->andWhere('o.number IS NOT NULL')
            ->andWhere('o.canceledBy IS NULL')
        ;
        if (null !== $directorate) {
            $qb
                ->andWhere('o.directorate = :directorate')
                ->setParameter('directorate', $directorate)
            ;
        }
        if (is_numeric($query)) {
            $qb
                ->andWhere('o.number = :number')
                ->setParameter('number', (int) $query)
            ;
        } else {
            $qb
                ->andWhere('o.title LIKE :title')
                ->setParameter('title', sprintf('%%%s%%', $query))
            ;
        }
        $qb
            ->setFirstResult($offset)
            ->setMaxResults($limit)
        ;

        return $qb->getQuery()->getResult();
    }

    /**
     * @return Order[]
     */
    public function findLatestByDirectorate(?Directorate $directorate = null, int $limit = 5): array
    {
        $qb = $this->createQueryBuilder('o');
        if ($directorate) {
            $qb
                ->where('o.directorate = :directorate')
                ->setParameter('directorate', $directorate)
            ;
        }

        return $qb
            ->andWhere('o.number IS NOT NULL')
            ->orderBy('o.signedAt', 'DESC')
            ->setMaxResults($limit)
            ->getQuery()
            ->getResult()
        ;
    }
}
