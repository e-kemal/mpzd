<?php

declare(strict_types=1);

namespace App\Repository;

use App\Entity\Directorate;
use App\Entity\HrOrder;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;
use Knp\Component\Pager\Pagination\PaginationInterface;
use Knp\Component\Pager\PaginatorInterface;

/**
 * @method HrOrder|null find($id, $lockMode = null, $lockVersion = null)
 * @method HrOrder|null findOneBy(array $criteria, array $orderBy = null)
 * @method HrOrder[]    findAll()
 * @method HrOrder[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class HrOrderRepository extends ServiceEntityRepository
{
    public function __construct(
        ManagerRegistry $registry,
        private PaginatorInterface $paginator,
    ) {
        parent::__construct($registry, HrOrder::class);
    }

    /**
     * @return PaginationInterface|HrOrder[]
     */
    public function paginateByDirectorate(Directorate $directorate, bool $signed = true, ?int $page = null): PaginationInterface|array
    {
        $qb = $this->createQueryBuilder('o')
            ->addSelect('a')
            ->addSelect('ap')
            ->addSelect('u')
            ->leftJoin('o.author', 'a')
            ->leftJoin('o.authorPosition', 'ap')
            ->leftJoin('a.user', 'u')
            ->where('o.directorate = :directorate')
            ->andWhere($signed ? 'o.number IS NOT NULL' : 'o.number IS NULL')
            ->setParameter('directorate', $directorate)
        ;
        if ($signed) {
            $qb->orderBy('o.year', 'DESC')->addOrderBy('o.number', 'DESC');
        } else {
            $qb->orderBy('o.createdAt', 'DESC');
        }
        if (null === $page) {
            return $qb->getQuery()->getResult();
        }

        return $this->paginator->paginate($qb, $page);
    }

    public function findMaxUsedNumberByOrder(HrOrder $order): ?int
    {
        return $this->createQueryBuilder('o')
            ->select('MAX(o.number)')
            ->where('o.directorate = :directorate')
            ->andWhere('o.year = :year')
            ->setParameter('directorate', $order->getDirectorate())
            ->setParameter('year', $order->getYear())
            ->getQuery()
            ->getSingleScalarResult()
        ;
    }

    public function findLatestByDirectorate(?Directorate $directorate = null, int $limit = 5): array
    {
        $qb = $this->createQueryBuilder('o');
        if ($directorate) {
            $qb
                ->where('o.directorate = :directorate')
                ->setParameter('directorate', $directorate)
            ;
        }

        return $qb
            ->andWhere('o.number IS NOT NULL')
            ->orderBy('o.signedAt', 'DESC')
            ->setMaxResults($limit)
            ->getQuery()
            ->getResult()
        ;
    }
}
