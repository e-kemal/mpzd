<?php

declare(strict_types=1);

namespace App\Repository;

use App\Entity\Admittance;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @extends ServiceEntityRepository<Admittance>
 *
 * @method Admittance|null find($id, $lockMode = null, $lockVersion = null)
 * @method Admittance|null findOneBy(array $criteria, array $orderBy = null)
 * @method Admittance[]    findAll()
 * @method Admittance[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class AdmittanceRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Admittance::class);
    }
}
