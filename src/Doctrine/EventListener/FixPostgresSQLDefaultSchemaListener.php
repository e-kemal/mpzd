<?php

declare(strict_types=1);

namespace App\Doctrine\EventListener;

use Doctrine\DBAL\Schema\PostgreSQLSchemaManager;
use Doctrine\ORM\Tools\Event\GenerateSchemaEventArgs;

class FixPostgresSQLDefaultSchemaListener
{
    public function postGenerateSchema(GenerateSchemaEventArgs $args)
    {
        $schemaManager = $args
            ->getEntityManager()
            ->getConnection()
            ->createSchemaManager()
        ;

        if ($schemaManager instanceof PostgreSQLSchemaManager) {
            foreach ($schemaManager->getExistingSchemaSearchPaths() as $path) {
                if (!$args->getSchema()->hasNamespace($path)) {
                    $args->getSchema()->createNamespace($path);
                }
            }
        }
    }
}
