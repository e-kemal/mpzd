<?php

declare(strict_types=1);

namespace App\Entity;

use App\Repository\OrderEmployeeItemRepository;
use App\Type\Rank;
use Doctrine\ORM\Mapping as ORM;
use Gedmo\Timestampable\Traits\TimestampableEntity;

#[ORM\Entity(repositoryClass: OrderEmployeeItemRepository::class)]
#[ORM\Table(name: 'order_employee_items')]
class OrderEmployeeItem
{
    use IdTrait;
    use TimestampableEntity;

    #[ORM\ManyToOne(targetEntity: HrOrder::class, inversedBy: 'employeeItems')]
    #[ORM\JoinColumn(nullable: false)]
    private HrOrder $order;

    #[ORM\ManyToOne(targetEntity: Employee::class, inversedBy: 'orderItems')]
    #[ORM\JoinColumn(nullable: false)]
    private Employee $employee;

    #[ORM\ManyToOne(targetEntity: Position::class)]
    private ?Position $oldPosition = null;

    #[ORM\ManyToOne(targetEntity: Position::class)]
    private ?Position $newPosition = null;

    #[ORM\Column(type: 'rank', nullable: true)]
    private Rank $oldRank;

    #[ORM\Column(type: 'rank', nullable: true)]
    private Rank $newRank;

    #[ORM\Column(type: 'integer')]
    private int $oldPoints = 0;

    #[ORM\Column(type: 'integer')]
    private int $newPoints = 0;

    #[ORM\Column(type: 'json')]
    private array $addAdmittanceIds = [];

    /** @var Admittance[] */
    private array $addAdmittances = [];

    #[ORM\Column(type: 'json')]
    private array $removeAdmittanceIds = [];

    /** @var Admittance[] */
    private array $removeAdmittances = [];

    #[ORM\Column(type: 'json')]
    private array $actualAdmittanceIds = [];

    public function __construct(HrOrder $order, Employee $employee)
    {
        $this->order = $order;
        $this->employee = $employee;
        $this->oldRank = new Rank(null);
        $this->newRank = new Rank(null);
    }

    public function __toString(): string
    {
        return (string) $this->employee;
    }

    public function getOrder(): HrOrder
    {
        return $this->order;
    }

    public function getEmployee(): Employee
    {
        return $this->employee;
    }

    public function getOldPosition(): ?Position
    {
        return $this->oldPosition;
    }

    public function setOldPosition(?Position $oldPosition): self
    {
        $this->oldPosition = $oldPosition;

        return $this;
    }

    public function getNewPosition(): ?Position
    {
        return $this->newPosition;
    }

    public function setNewPosition(?Position $newPosition): self
    {
        $this->newPosition = $newPosition;

        return $this;
    }

    public function getOldRank(): Rank
    {
        return $this->oldRank;
    }

    public function setOldRank($oldRank): self
    {
        $this->oldRank = $oldRank;

        return $this;
    }

    public function getNewRank(): Rank
    {
        return $this->newRank;
    }

    public function setNewRank($newRank): self
    {
        $this->newRank = $newRank;

        return $this;
    }

    public function getOldPoints(): int
    {
        return $this->oldPoints;
    }

    public function setOldPoints(int $oldPoints): self
    {
        $this->oldPoints = $oldPoints;

        return $this;
    }

    public function getNewPoints(): int
    {
        return $this->newPoints;
    }

    public function setNewPoints(int $newPoints): self
    {
        $this->newPoints = $newPoints;

        return $this;
    }

    public function getAddAdmittanceIds(): array
    {
        return $this->addAdmittanceIds;
    }

    public function setAddAdmittanceIds(array $addAdmittanceIds): self
    {
        $this->addAdmittanceIds = $addAdmittanceIds;

        return $this;
    }

    /**
     * @return Admittance[]
     */
    public function getAddAdmittances(): array
    {
        return $this->addAdmittances;
    }

    /**
     * @param Admittance[] $addAdmittances
     */
    public function setAddAdmittances(array $addAdmittances): self
    {
        $this->addAdmittances = $addAdmittances;

        return $this;
    }

    public function getRemoveAdmittanceIds(): array
    {
        return $this->removeAdmittanceIds;
    }

    public function setRemoveAdmittanceIds(array $removeAdmittanceIds): self
    {
        $this->removeAdmittanceIds = $removeAdmittanceIds;

        return $this;
    }

    /**
     * @return Admittance[]
     */
    public function getRemoveAdmittances(): array
    {
        return $this->removeAdmittances;
    }

    /**
     * @param Admittance[] $removeAdmittances
     */
    public function setRemoveAdmittances(array $removeAdmittances): self
    {
        $this->removeAdmittances = $removeAdmittances;

        return $this;
    }

    public function getActualAdmittanceIds(): array
    {
        return $this->actualAdmittanceIds;
    }

    public function setActualAdmittanceIds(array $actualAdmittanceIds): self
    {
        $this->actualAdmittanceIds = $actualAdmittanceIds;

        return $this;
    }
}
