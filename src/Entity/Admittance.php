<?php

declare(strict_types=1);

namespace App\Entity;

use App\Repository\AdmittanceRepository;
use Doctrine\ORM\Mapping as ORM;
use Gedmo\Timestampable\Traits\TimestampableEntity;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;
use Symfony\Component\Validator\Constraints as Assert;

#[ORM\Entity(repositoryClass: AdmittanceRepository::class)]
#[UniqueEntity(fields: ['directorate', 'name'], errorPath: 'name')]
#[ORM\Table(name: 'admittances')]
class Admittance
{
    use IdTrait;
    use TimestampableEntity;

    #[ORM\ManyToOne]
    #[ORM\JoinColumn(nullable: false)]
    private Directorate $directorate;

    #[ORM\Column(type: 'text')]
    #[Assert\NotBlank]
    private string $name = '';

    public function __construct(Directorate $directorate)
    {
        $this->directorate = $directorate;
    }

    public function __toString(): string
    {
        return $this->name;
    }

    public function getDirectorate(): Directorate
    {
        return $this->directorate;
    }

    public function getName(): string
    {
        return $this->name;
    }

    public function setName(string $name): self
    {
        $this->name = $name;

        return $this;
    }
}
