<?php

declare(strict_types=1);

namespace App\Entity;

use App\Repository\PositionRepository;
use Doctrine\ORM\Mapping as ORM;
use Gedmo\Timestampable\Traits\TimestampableEntity;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;
use Symfony\Component\Validator\Constraints as Assert;

#[ORM\Entity(repositoryClass: PositionRepository::class)]
#[UniqueEntity(fields: ['directorate', 'name'], errorPath: 'name')]
#[UniqueEntity(fields: ['shortName'])]
#[ORM\Table(name: 'positions')]
class Position
{
    use IdTrait;
    use TimestampableEntity;

    #[ORM\ManyToOne(targetEntity: Directorate::class, inversedBy: 'positions')]
    #[ORM\JoinColumn(nullable: false)]
    private Directorate $directorate;

    #[ORM\Column(type: 'text')]
    #[Assert\NotBlank]
    private string $name = '';

    #[ORM\Column(type: 'text')]
    #[Assert\NotBlank]
    private string $shortName = '';

    #[ORM\Column(type: 'smallint')]
    private int $accessLevel = 0;

    #[ORM\Column(type: 'string', length: 255)]
    private string $shoulderMarkFileName = '';

    public function __construct(Directorate $directorate)
    {
        $this->directorate = $directorate;
    }

    public function __toString(): string
    {
        return sprintf('%s - %s', $this->shortName, $this->name);
    }

    public function getDirectorate(): Directorate
    {
        return $this->directorate;
    }

    public function getName(): string
    {
        return $this->name;
    }

    public function setName(string $name): self
    {
        $this->name = $name;

        return $this;
    }

    public function getShortName(): string
    {
        return $this->shortName;
    }

    public function setShortName(string $shortName): self
    {
        $this->shortName = $shortName;

        return $this;
    }

    public function getAccessLevel(): int
    {
        return $this->accessLevel;
    }

    public function setAccessLevel(int $accessLevel): self
    {
        $this->accessLevel = $accessLevel;

        return $this;
    }

    public function getShoulderMarkFileName(): string
    {
        return $this->shoulderMarkFileName;
    }

    public function setShoulderMarkFileName(string $shoulderMarkFileName): self
    {
        $this->shoulderMarkFileName = $shoulderMarkFileName;

        return $this;
    }
}
