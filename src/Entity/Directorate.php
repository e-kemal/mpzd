<?php

declare(strict_types=1);

namespace App\Entity;

use App\Repository\DirectorateRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\Common\Collections\Criteria;
use Doctrine\ORM\Mapping as ORM;
use Gedmo\Timestampable\Traits\TimestampableEntity;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;
use Symfony\Component\Validator\Constraints as Assert;

#[ORM\Entity(repositoryClass: DirectorateRepository::class)]
#[UniqueEntity(fields: ['name'])]
#[UniqueEntity(fields: ['shortName'])]
#[UniqueEntity(fields: ['alias'])]
#[ORM\Table(name: 'directorates')]
class Directorate
{
    use IdTrait;
    use TimestampableEntity;

    #[ORM\Column(type: 'text')]
    #[Assert\NotBlank]
    private string $name = '';

    #[ORM\Column(type: 'text')]
    #[Assert\NotBlank]
    private string $shortName = '';

    #[ORM\Column(type: 'text', unique: true)]
    #[Assert\NotBlank]
    #[Assert\Regex(pattern: '/^[0-9a-z]*$/')]
    private string $alias = '';

    #[ORM\Column(type: 'boolean', options: ['default' => false])]
    private bool $root = false;

    #[Assert\NotBlank]
    #[ORM\Column(type: 'integer')]
    private int $ordersTopicId = 0;

    #[Assert\NotBlank]
    #[ORM\Column(type: 'integer')]
    private int $hrOrdersTopicId = 0;

    #[ORM\Column(type: 'integer')]
    private int $employeesNumberFieldId = 0;

    #[ORM\Column(type: 'integer')]
    private int $employeesShoulderMarkFieldId = 0;

    #[ORM\Column(type: 'integer')]
    private int $employeesPointsFieldId = 0;

    #[ORM\Column(type: 'integer')]
    private int $employeesAdmittancesFieldId = 0;

    #[ORM\OneToMany(mappedBy: 'directorate', targetEntity: Position::class)]
    private Collection $positions;

    #[ORM\OneToMany(mappedBy: 'directorate', targetEntity: Employee::class, fetch: 'EXTRA_LAZY')]
    private Collection $employees;

    #[ORM\OneToMany(mappedBy: 'directorate', targetEntity: Order::class)]
    private Collection $orders;

    #[ORM\OneToMany(mappedBy: 'directorate', targetEntity: HrOrder::class)]
    private Collection $hrOrders;

    public function __construct()
    {
        $this->positions = new ArrayCollection();
        $this->employees = new ArrayCollection();
        $this->orders = new ArrayCollection();
        $this->hrOrders = new ArrayCollection();
    }

    public function __toString(): string
    {
        return $this->name;
    }

    public function getName(): string
    {
        return $this->name;
    }

    public function setName(string $name): self
    {
        $this->name = $name;

        return $this;
    }

    public function getShortName(): string
    {
        return $this->shortName;
    }

    public function setShortName(string $shortName): self
    {
        $this->shortName = $shortName;

        return $this;
    }

    public function getAlias(): string
    {
        return $this->alias;
    }

    public function setAlias(string $alias): self
    {
        $this->alias = $alias;

        return $this;
    }

    public function isRoot(): bool
    {
        return $this->root;
    }

    public function setRoot(bool $root): self
    {
        $this->root = $root;

        return $this;
    }

    public function getOrdersTopicId(): int
    {
        return $this->ordersTopicId;
    }

    public function setOrdersTopicId(int $ordersTopicId): self
    {
        $this->ordersTopicId = $ordersTopicId;

        return $this;
    }

    public function getHrOrdersTopicId(): int
    {
        return $this->hrOrdersTopicId;
    }

    public function setHrOrdersTopicId(int $hrOrdersTopicId): self
    {
        $this->hrOrdersTopicId = $hrOrdersTopicId;

        return $this;
    }

    public function getEmployeesNumberFieldId(): int
    {
        return $this->employeesNumberFieldId;
    }

    public function setEmployeesNumberFieldId(int $employeesNumberFieldId): self
    {
        $this->employeesNumberFieldId = $employeesNumberFieldId;

        return $this;
    }

    public function getEmployeesShoulderMarkFieldId(): int
    {
        return $this->employeesShoulderMarkFieldId;
    }

    public function setEmployeesShoulderMarkFieldId(int $employeesShoulderMarkFieldId): self
    {
        $this->employeesShoulderMarkFieldId = $employeesShoulderMarkFieldId;

        return $this;
    }

    public function getEmployeesPointsFieldId(): int
    {
        return $this->employeesPointsFieldId;
    }

    public function setEmployeesPointsFieldId(int $employeesPointsFieldId): self
    {
        $this->employeesPointsFieldId = $employeesPointsFieldId;

        return $this;
    }

    public function getEmployeesAdmittancesFieldId(): int
    {
        return $this->employeesAdmittancesFieldId;
    }

    public function setEmployeesAdmittancesFieldId(int $employeesAdmittancesFieldId): self
    {
        $this->employeesAdmittancesFieldId = $employeesAdmittancesFieldId;

        return $this;
    }

    /**
     * @return Position[]
     */
    public function getPositions(): array
    {
        return $this->positions->toArray();
    }

    /**
     * @return Employee[]
     */
    public function getEmployees(): array
    {
        return $this->employees->toArray();
    }

    /**
     * @deprecated
     */
    public function getEmployeeByUser(User $user): ?Employee
    {
        $criteria = Criteria::create();
        $criteria->andWhere(Criteria::expr()->eq('user', $user));

        return $this->employees->matching($criteria)->first() ?: null;
    }

    /**
     * @return Order[]
     */
    public function getOrders(): array
    {
        return $this->orders->toArray();
    }

    /**
     * @return HrOrder[]
     */
    public function getHrOrders(): array
    {
        return $this->hrOrders->toArray();
    }
}
