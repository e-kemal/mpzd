<?php

declare(strict_types=1);

namespace App\Entity;

use App\Repository\EmployeeRepository;
use App\Type\Rank;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Gedmo\Timestampable\Traits\TimestampableEntity;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;
use Symfony\Component\Validator\Constraints as Assert;
use Symfony\Component\Validator\Context\ExecutionContextInterface;

#[ORM\Entity(repositoryClass: EmployeeRepository::class)]
#[UniqueEntity(fields: ['directorate', 'user'])]
#[UniqueEntity(fields: ['directorate', 'number'], errorPath: 'number')]
#[ORM\Table(name: 'employees')]
#[ORM\UniqueConstraint(fields: ['user', 'directorate'])]
#[ORM\UniqueConstraint(fields: ['directorate', 'number'])]
class Employee
{
    use IdTrait;
    use TimestampableEntity;

    #[ORM\ManyToOne(targetEntity: User::class, fetch: 'EAGER', inversedBy: 'employees')]
    #[ORM\JoinColumn(nullable: false)]
    private User $user;

    #[ORM\ManyToOne(targetEntity: Directorate::class, fetch: 'EAGER', inversedBy: 'employees')]
    #[ORM\JoinColumn(nullable: false)]
    private Directorate $directorate;

    #[ORM\Column(type: 'integer')]
    #[Assert\NotBlank]
    private int $number = 0;

    #[ORM\ManyToOne(targetEntity: Position::class, fetch: 'EAGER')]
    private ?Position $position = null;

    #[ORM\Column(type: 'rank', nullable: true)]
    private Rank $rank;

    #[ORM\Column(type: 'integer')]
    private int $points = 0;

    /** @var Collection<int, Admittance> */
    #[ORM\ManyToMany(targetEntity: Admittance::class)]
    #[ORM\JoinTable(name: 'employee_admittances')]
    #[ORM\OrderBy(['createdAt' => 'ASC'])]
    private Collection $admittances;

    #[ORM\OneToMany(mappedBy: 'employee', targetEntity: OrderEmployeeItem::class)]
    private Collection $orderItems;

    public function __construct(User $user, Directorate $directorate)
    {
        $this->user = $user;
        $this->directorate = $directorate;
        $this->rank = new Rank(null);
        $this->admittances = new ArrayCollection();
        $this->orderItems = new ArrayCollection();
    }

    public function __toString(): string
    {
        return $this->directorate.' #'.$this->number.' '.$this->user;
    }

    public function getUser(): User
    {
        return $this->user;
    }

    public function getDirectorate(): Directorate
    {
        return $this->directorate;
    }

    public function getNumber(): int
    {
        return $this->number;
    }

    public function setNumber(int $number): self
    {
        $this->number = $number;

        return $this;
    }

    public function getPosition(): ?Position
    {
        return $this->position;
    }

    public function setPosition(?Position $position): self
    {
        $this->position = $position;

        return $this;
    }

    public function getRank(): Rank
    {
        return $this->rank;
    }

    public function setRank(Rank $rank): self
    {
        $this->rank = $rank;

        return $this;
    }

    public function getPoints(): int
    {
        return $this->points;
    }

    public function setPoints(int $points): self
    {
        $this->points = $points;

        return $this;
    }

    /**
     * @return Admittance[]
     */
    public function getAdmittances(): array
    {
        return $this->admittances->toArray();
    }

    public function containsAdmittance(Admittance $admittance): bool
    {
        return $this->admittances->contains($admittance);
    }

    public function addAdmittance(Admittance $admittance): self
    {
        if (!$this->admittances->contains($admittance)) {
            $this->admittances->add($admittance);
        }

        return $this;
    }

    public function removeAdmittance(Admittance $admittance): self
    {
        $this->admittances->removeElement($admittance);

        return $this;
    }

    #[Assert\Callback]
    public function validateDirectorate(ExecutionContextInterface $context)
    {
        if (null !== $this->position && !$this->directorate->getId()->equals($this->position->getDirectorate()->getId())) {
            $context->buildViolation('employee.position.belongs_to_another_directorate')->atPath('position')->addViolation();
        }
        foreach ($this->admittances as $index => $admittance) {
            if (!$this->directorate->getId()->equals($admittance->getDirectorate()->getId())) {
                $context
                    ->buildViolation('employee.admittances.belongs_to_another_directorate')
                    ->atPath(sprintf('admittances[%s]', $index))
                    ->addViolation()
                ;
            }
        }
    }

    /**
     * @return OrderEmployeeItem[]
     */
    public function getOrderItems(): array
    {
        return $this->orderItems->toArray();
    }
}
