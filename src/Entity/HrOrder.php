<?php

declare(strict_types=1);

namespace App\Entity;

use App\Repository\HrOrderRepository;
use App\Type\Rank;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Gedmo\Timestampable\Traits\TimestampableEntity;

#[ORM\Entity(repositoryClass: HrOrderRepository::class)]
#[ORM\Table(name: 'hr_orders')]
#[ORM\UniqueConstraint(fields: ['directorate', 'year', 'number'])]
class HrOrder
{
    use IdTrait;
    use TimestampableEntity;

    #[ORM\ManyToOne(targetEntity: Directorate::class, inversedBy: 'hrOrders')]
    #[ORM\JoinColumn(nullable: false)]
    private Directorate $directorate;

    #[ORM\Column(type: 'integer')]
    private int $year;

    #[ORM\Column(type: 'integer', nullable: true)]
    private ?int $number = null;

    #[ORM\ManyToOne(targetEntity: Employee::class)]
    #[ORM\JoinColumn(nullable: false)]
    private Employee $author;

    #[ORM\ManyToOne(targetEntity: Position::class)]
    #[ORM\JoinColumn(nullable: false)]
    private Position $authorPosition;

    #[ORM\Column(type: 'rank', nullable: true)]
    private Rank $authorRank;

    #[ORM\Column(type: 'text')]
    private string $text = '';

    #[ORM\Column(type: 'datetime_immutable', nullable: true)]
    private ?\DateTimeImmutable $signedAt;

    #[ORM\Column(type: 'json')]
    private array $draftData = [];

    #[ORM\Column(type: 'integer', nullable: true)]
    private ?int $externalId = null;

    #[ORM\Column(type: 'text', nullable: true)]
    private ?string $externalUrl = null;

    #[ORM\OneToMany(mappedBy: 'order', targetEntity: OrderEmployeeItem::class)]
    private Collection $employeeItems;

    public function __construct(Employee $author)
    {
        $this->directorate = $author->getDirectorate();
        $this->year = (int) (new \DateTimeImmutable())->format('Y');
        $this->author = $author;
        $this->authorPosition = $author->getPosition();
        $this->authorRank = $author->getRank();
        $this->employeeItems = new ArrayCollection();
    }

    public function __toString(): string
    {
        if (null !== $this->number) {
            return sprintf('Приказ №%s от %s', $this->getNumberString(), $this->signedAt->format('d.m.Y H:i:s'));
        }

        return sprintf('Черновик приказа от %s', $this->createdAt->format('d.m.Y H:i:s'));
    }

    public function getDirectorate(): Directorate
    {
        return $this->directorate;
    }

    public function setDirectorate(Directorate $directorate): self
    {
        $this->directorate = $directorate;

        return $this;
    }

    public function getYear(): int
    {
        return $this->year;
    }

    public function setYear(int $year): self
    {
        $this->year = $year;

        return $this;
    }

    public function getNumber(): ?int
    {
        return $this->number;
    }

    public function setNumber(?int $number): self
    {
        $this->number = $number;

        return $this;
    }

    public function getNumberString(): string
    {
        return $this->number.'/К';
    }

    public function getAuthor(): Employee
    {
        return $this->author;
    }

    public function setAuthor(Employee $author): self
    {
        $this->author = $author;

        return $this;
    }

    public function getAuthorPosition(): Position
    {
        return $this->authorPosition;
    }

    public function setAuthorPosition(Position $authorPosition): self
    {
        $this->authorPosition = $authorPosition;

        return $this;
    }

    public function getAuthorRank(): Rank
    {
        return $this->authorRank;
    }

    public function setAuthorRank(Rank $authorRank): self
    {
        $this->authorRank = $authorRank;

        return $this;
    }

    public function getText(): string
    {
        return $this->text;
    }

    public function setText(string $text): self
    {
        $this->text = $text;

        return $this;
    }

    public function getSignedAt(): ?\DateTimeImmutable
    {
        return $this->signedAt;
    }

    public function setSignedAt(?\DateTimeImmutable $signedAt): self
    {
        $this->signedAt = $signedAt;

        return $this;
    }

    public function getDraftData(): array
    {
        return $this->draftData;
    }

    public function setDraftData(array $draftData): self
    {
        $this->draftData = $draftData;

        return $this;
    }

    public function getExternalId(): ?int
    {
        return $this->externalId;
    }

    public function setExternalId(?int $externalId): self
    {
        $this->externalId = $externalId;

        return $this;
    }

    public function getExternalUrl(): ?string
    {
        return $this->externalUrl;
    }

    public function setExternalUrl(?string $externalUrl): self
    {
        $this->externalUrl = $externalUrl;

        return $this;
    }

    /**
     * @return OrderEmployeeItem[]
     */
    public function getEmployeeItems(): array
    {
        return $this->employeeItems->toArray();
    }

    public function createEmployeeItem(Employee $employee): OrderEmployeeItem
    {
        $employeeItem = new OrderEmployeeItem($this, $employee);
        $this->employeeItems[] = $employeeItem;

        return $employeeItem;
    }
}
