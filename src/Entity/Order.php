<?php

declare(strict_types=1);

namespace App\Entity;

use App\Repository\OrderRepository;
use App\Type\Rank;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Gedmo\Timestampable\Traits\TimestampableEntity;

#[ORM\Entity(repositoryClass: OrderRepository::class)]
#[ORM\Table(name: 'orders')]
#[ORM\UniqueConstraint(fields: ['directorate', 'number'])]
class Order
{
    use IdTrait;
    use TimestampableEntity;

    #[ORM\ManyToOne(targetEntity: Directorate::class, inversedBy: 'orders')]
    #[ORM\JoinColumn(nullable: false)]
    private Directorate $directorate;

    #[ORM\Column(type: 'integer', nullable: true)]
    private ?int $number = null;

    #[ORM\ManyToOne(targetEntity: Employee::class)]
    #[ORM\JoinColumn(nullable: false)]
    private Employee $author;

    #[ORM\ManyToOne(targetEntity: Position::class)]
    #[ORM\JoinColumn(nullable: false)]
    private Position $authorPosition;

    #[ORM\Column(type: 'rank', nullable: true)]
    private Rank $authorRank;

    #[ORM\Column(type: 'text')]
    private string $title = '';

    #[ORM\Column(type: 'text')]
    private string $text = '';

    #[ORM\Column(type: 'datetime_immutable', nullable: true)]
    private ?\DateTimeImmutable $signedAt = null;

    #[ORM\ManyToOne(targetEntity: self::class, inversedBy: 'cancelOrders')]
    private ?Order $canceledBy = null;

    #[ORM\OneToMany(mappedBy: 'canceledBy', targetEntity: self::class)]
    #[ORM\OrderBy(['number' => 'ASC'])]
    private Collection $cancelOrders;

    #[ORM\Column(type: 'json')]
    private array $draftData = [];

    #[ORM\Column(type: 'integer', nullable: true)]
    private ?int $externalId = null;

    #[ORM\Column(type: 'text', nullable: true)]
    private ?string $externalUrl = null;

    public function __construct(Employee $author)
    {
        $this->directorate = $author->getDirectorate();
        $this->author = $author;
        $this->authorPosition = $author->getPosition();
        $this->authorRank = $author->getRank();
        $this->cancelOrders = new ArrayCollection();
    }

    public function __toString(): string
    {
        $str = $this->getCaption().' '.$this->title;
        if (mb_strlen($str) > 100) {
            return mb_substr($str, 0, 97).'...';
        }

        return $str;
    }

    public function getCaption(): string
    {
        if (null !== $this->number) {
            return sprintf('Приказ №%s от %s', $this->getNumberString(), $this->signedAt->format('d.m.Y H:i:s'));
        }

        return sprintf('Черновик приказа от %s', $this->createdAt->format('d.m.Y H:i:s'));
    }

    public function getDirectorate(): Directorate
    {
        return $this->directorate;
    }

    public function setDirectorate(Directorate $directorate): self
    {
        $this->directorate = $directorate;

        return $this;
    }

    public function getNumber(): ?int
    {
        return $this->number;
    }

    public function setNumber(?int $number): self
    {
        $this->number = $number;

        return $this;
    }

    public function getNumberString(): string
    {
        return $this->number.'/'.$this->authorPosition->getShortName();
    }

    public function getAuthor(): Employee
    {
        return $this->author;
    }

    public function setAuthor(Employee $author): self
    {
        $this->author = $author;

        return $this;
    }

    public function getAuthorPosition(): Position
    {
        return $this->authorPosition;
    }

    public function setAuthorPosition(Position $authorPosition): self
    {
        $this->authorPosition = $authorPosition;

        return $this;
    }

    public function getAuthorRank(): Rank
    {
        return $this->authorRank;
    }

    public function setAuthorRank(Rank $authorRank): self
    {
        $this->authorRank = $authorRank;

        return $this;
    }

    public function getTitle(): string
    {
        return $this->title;
    }

    public function setTitle(string $title): self
    {
        $this->title = $title;

        return $this;
    }

    public function getText(): string
    {
        return $this->text;
    }

    public function setText(string $text): self
    {
        $this->text = $text;

        return $this;
    }

    public function getSignedAt(): ?\DateTimeImmutable
    {
        return $this->signedAt;
    }

    public function setSignedAt(?\DateTimeImmutable $signedAt): self
    {
        $this->signedAt = $signedAt;

        return $this;
    }

    public function getCanceledBy(): ?self
    {
        return $this->canceledBy;
    }

    public function setCanceledBy(?self $canceledBy): self
    {
        $this->canceledBy = $canceledBy;

        return $this;
    }

    /**
     * @return self[]
     */
    public function getCancelOrders(): array
    {
        return $this->cancelOrders->toArray();
    }

    public function addCancelOrder(self $cancelOrder): self
    {
        if (!$this->cancelOrders->contains($cancelOrder)) {
            $this->cancelOrders[] = $cancelOrder;
            $cancelOrder->setCanceledBy($this);
        }

        return $this;
    }

    public function removeCancelOrder(self $cancelOrder): self
    {
        if ($this->cancelOrders->removeElement($cancelOrder)) {
            // set the owning side to null (unless already changed)
            if ($cancelOrder->getCanceledBy() === $this) {
                $cancelOrder->setCanceledBy(null);
            }
        }

        return $this;
    }

    public function getDraftData(): array
    {
        return $this->draftData;
    }

    public function setDraftData(array $draftData): self
    {
        $this->draftData = $draftData;

        return $this;
    }

    public function getExternalId(): ?int
    {
        return $this->externalId;
    }

    public function setExternalId(?int $externalId): self
    {
        $this->externalId = $externalId;

        return $this;
    }

    public function getExternalUrl(): ?string
    {
        return $this->externalUrl;
    }

    public function setExternalUrl(?string $externalUrl): self
    {
        $this->externalUrl = $externalUrl;

        return $this;
    }
}
