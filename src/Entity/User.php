<?php

declare(strict_types=1);

namespace App\Entity;

use App\Repository\UserRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Gedmo\Timestampable\Traits\TimestampableEntity;
use League\OAuth2\Client\Token\AccessToken;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;
use Symfony\Component\Security\Core\User\PasswordAuthenticatedUserInterface;
use Symfony\Component\Security\Core\User\UserInterface;

#[ORM\Entity(repositoryClass: UserRepository::class)]
#[UniqueEntity(fields: ['username'])]
#[UniqueEntity(fields: ['externalId'])]
#[ORM\Table(name: 'users')]
class User implements UserInterface, PasswordAuthenticatedUserInterface
{
    use IdTrait;
    use TimestampableEntity;

    #[ORM\Column(type: 'text', unique: true)]
    private string $username = '';

    #[ORM\Column(type: 'text')]
    private string $password = '';

    private ?string $plainPassword = null;

    #[ORM\Column(type: 'boolean')]
    private bool $admin = false;

    /**
     * @var string[]
     */
    private array $roles = [];

    #[ORM\Column(type: 'integer', unique: true, nullable: true)]
    private ?int $externalId = null;

    #[ORM\Column(type: 'text', nullable: true)]
    private ?string $oauthAccessToken = null;

    #[ORM\Column(type: 'datetime', nullable: true)]
    private ?\DateTimeInterface $oauthExpires = null;

    #[ORM\Column(type: 'text', nullable: true)]
    private ?string $oauthRefreshToken = null;

    /** @var string[]|null */
    #[ORM\Column(type: 'simple_array', nullable: true)]
    private ?array $oauthScopes = null;

    #[ORM\OneToMany(mappedBy: 'user', targetEntity: Employee::class, indexBy: 'directorate_id')]
    private Collection $employees;

    public function __construct()
    {
        $this->employees = new ArrayCollection();
    }

    public function __toString(): string
    {
        return $this->username;
    }

    public function getUserIdentifier(): string
    {
        return $this->username;
    }

    public function getUsername(): string
    {
        return $this->username;
    }

    public function setUsername(string $username): self
    {
        $this->username = $username;

        return $this;
    }

    public function getPassword(): string
    {
        return $this->password;
    }

    public function setPassword(string $password): self
    {
        $this->password = $password;

        return $this;
    }

    public function getPlainPassword(): ?string
    {
        return $this->plainPassword;
    }

    public function setPlainPassword(?string $plainPassword): self
    {
        $this->plainPassword = $plainPassword;

        return $this;
    }

    public function getRoles(): array
    {
        $roles = $this->roles;
        $roles[] = 'ROLE_USER';
        if ($this->admin) {
            $roles[] = 'ROLE_ADMIN';
        }

        return array_unique($roles);
    }

    /**
     * @param string[] $roles
     */
    public function setRoles(array $roles): self
    {
        $this->roles = $roles;

        return $this;
    }

    public function eraseCredentials()
    {
        $this->plainPassword = null;
    }

    public function isAdmin(): bool
    {
        return $this->admin;
    }

    public function setAdmin(bool $admin): self
    {
        $this->admin = $admin;

        return $this;
    }

    public function getExternalId(): ?int
    {
        return $this->externalId;
    }

    public function setExternalId(?int $externalId): self
    {
        $this->externalId = $externalId;

        return $this;
    }

    public function getAccessToken(): ?AccessToken
    {
        if (null === $this->oauthAccessToken) {
            return null;
        }

        return new AccessToken([
            'access_token' => $this->oauthAccessToken,
            'expires' => $this->oauthExpires?->getTimestamp(),
            'refresh_token' => $this->oauthRefreshToken,
            'scope' => implode(' ', $this->oauthScopes),
        ]);
    }

    public function setAccessToken(?AccessToken $token): self
    {
        if ($token) {
            $this->oauthAccessToken = $token->getToken();
            $this->oauthExpires = new \DateTime();
            $this->oauthExpires->setTimestamp($token->getExpires());
            $this->oauthRefreshToken = $token->getRefreshToken();
            $this->oauthScopes = explode(' ', $token->getValues()['scope']);
        } else {
            $this->oauthAccessToken = null;
            $this->oauthExpires = null;
            $this->oauthRefreshToken = null;
            $this->oauthScopes = null;
        }

        return $this;
    }

    /**
     * @return Employee[]
     */
    public function getEmployees(): array
    {
        return $this->employees->toArray();
    }
}
