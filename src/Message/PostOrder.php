<?php

declare(strict_types=1);

namespace App\Message;

use Symfony\Component\Uid\Uuid;

final class PostOrder
{
    public function __construct(
        private Uuid $orderId,
    ) {
    }

    public function getOrderId(): Uuid
    {
        return $this->orderId;
    }
}
