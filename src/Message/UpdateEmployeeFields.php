<?php

declare(strict_types=1);

namespace App\Message;

use Symfony\Component\Uid\Uuid;

final class UpdateEmployeeFields
{
    public function __construct(
        private Uuid $employeeId,
    ) {
    }

    public function getEmployeeId(): Uuid
    {
        return $this->employeeId;
    }
}
