<?php

declare(strict_types=1);

namespace App\Form;

use App\Dto\DraftHrOrder;
use App\Dto\DraftHrOrderItem;
use App\Entity\Directorate;
use FOS\CKEditorBundle\Form\Type\CKEditorType;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\CollectionType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class DraftHrOrderType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        if ($options['directorate']?->isRoot()) {
            $builder->add('directorate', EntityType::class, [
                'class' => Directorate::class,
            ]);
        }
        $builder
            ->add('text', CKEditorType::class, [
                'empty_data' => '',
                'config' => [
                    'toolbar' => 'basic',
                ],
                'required' => true,
            ])
            ->add('items', CollectionType::class, [
                'allow_add' => true,
                'allow_delete' => true,
                'entry_type' => DraftHrOrderItemType::class,
                'entry_options' => [
                    'directorate' => $options['directorate'],
                ],
                'prototype' => true,
                'prototype_data' => new DraftHrOrderItem(),
            ])
        ;
    }

    public function configureOptions(OptionsResolver $resolver): void
    {
        $resolver->setDefaults([
            'data_class' => DraftHrOrder::class,
            'directorate' => null,
        ]);
    }
}
