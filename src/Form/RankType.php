<?php

declare(strict_types=1);

namespace App\Form;

use App\Type\Rank;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\CallbackTransformer;
use Symfony\Component\Form\Extension\Core\Type\IntegerType;
use Symfony\Component\Form\FormBuilderInterface;

class RankType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->addModelTransformer(new CallbackTransformer(
                fn (?Rank $rank) => $rank?->getValue(),
                fn (?int $value) => new Rank($value)
            ))
        ;
    }

    public function getParent(): string
    {
        return IntegerType::class;
    }
}
