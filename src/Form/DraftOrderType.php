<?php

declare(strict_types=1);

namespace App\Form;

use App\Dto\DraftOrder;
use App\Entity\Order;
use EasyCorp\Bundle\EasyAdminBundle\Form\Type\CrudAutocompleteType;
use FOS\CKEditorBundle\Form\Type\CKEditorType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\CollectionType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class DraftOrderType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        $builder
            ->add('title', TextType::class, ['required' => true])
            ->add('text', CKEditorType::class, [
                'empty_data' => '',
                'config' => [
                    'toolbar' => 'basic',
                ],
                'required' => true,
            ])
            ->add('cancelOrders', CollectionType::class, [
                'allow_add' => true,
                'allow_delete' => true,
                'delete_empty' => true,
                'entry_type' => CrudAutocompleteType::class,
                'entry_options' => [
                    'class' => Order::class,
                    'label' => false,
                ],
                'prototype' => true,
            ])
        ;
    }

    public function configureOptions(OptionsResolver $resolver): void
    {
        $resolver->setDefaults([
            'data_class' => DraftOrder::class,
        ]);
    }
}
