<?php

declare(strict_types=1);

namespace App\Form;

use App\Dto\DraftHrOrderItem;
use App\Entity\Admittance;
use App\Entity\Directorate;
use App\Entity\Position;
use App\Entity\User;
use App\Repository\AdmittanceRepository;
use App\Repository\PositionRepository;
use EasyCorp\Bundle\EasyAdminBundle\Form\Type\CrudAutocompleteType;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\IntegerType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class DraftHrOrderItemType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        /** @var Directorate|null $directorate */
        $directorate = $options['directorate'];
        $admittanceQB = function (AdmittanceRepository $repository) use ($directorate) {
            if ($directorate && !$directorate->isRoot()) {
                return $repository
                    ->createQueryBuilder('a')
                    ->where('a.directorate = :directorate')
                    ->setParameter('directorate', $directorate)
                ;
            }

            return null;
        };
        $builder
            ->add('user', CrudAutocompleteType::class, [
                'attr' => [
                    'class' => 'select-user',
                ],
                'class' => User::class,
                'label' => false,
            ])
            ->add('changePosition', CheckboxType::class, ['required' => false])
            ->add('position', EntityType::class, [
                'class' => Position::class,
                'empty_data' => null,
                'query_builder' => function (PositionRepository $repository) use ($directorate) {
                    if ($directorate && !$directorate->isRoot()) {
                        return $repository
                            ->createQueryBuilder('p')
                            ->where('p.directorate = :directorate')
                            ->setParameter('directorate', $directorate)
                        ;
                    }

                    return null;
                },
                'required' => false,
            ])
            ->add('changeRank', CheckboxType::class, ['required' => false])
            ->add('rank', RankType::class, ['required' => false])
            ->add('setPoints', ChoiceType::class, [
                'choices' => [
                    'add' => false,
                    'set' => true,
                ],
                'expanded' => true,
                'multiple' => false,
            ])
            ->add('points', IntegerType::class)
            ->add('addAdmittances', EntityType::class, [
                'class' => Admittance::class,
                'multiple' => true,
                'expanded' => true,
                'query_builder' => $admittanceQB,
            ])
            ->add('removeAdmittances', EntityType::class, [
                'class' => Admittance::class,
                'multiple' => true,
                'expanded' => true,
                'query_builder' => $admittanceQB,
            ])
        ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => DraftHrOrderItem::class,
            'directorate' => null,
        ]);
    }
}
