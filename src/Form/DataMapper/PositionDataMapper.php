<?php

declare(strict_types=1);

namespace App\Form\DataMapper;

use App\Entity\Position;
use Symfony\Component\Form\DataMapperInterface;

class PositionDataMapper implements DataMapperInterface
{
    /**
     * {@inheritdoc}
     */
    public function mapDataToForms(mixed $viewData, \Traversable $forms)
    {
        $forms = iterator_to_array($forms);
        if ($viewData instanceof Position) {
            $forms['directorate']->setData($viewData->getDirectorate());
            $forms['name']->setData($viewData->getName());
            $forms['shortName']->setData($viewData->getShortName());
            $forms['accessLevel']->setData($viewData->getAccessLevel());
        } else {
            $forms['accessLevel']->setData(0);
        }
    }

    /**
     * {@inheritdoc}
     */
    public function mapFormsToData(\Traversable $forms, mixed &$viewData)
    {
        $forms = iterator_to_array($forms);

        if (!$viewData instanceof Position) {
            $viewData = new Position($forms['directorate']->getData());
        }
        $viewData->setName($forms['name']->getData());
        $viewData->setShortName($forms['shortName']->getData());
        $viewData->setAccessLevel($forms['accessLevel']->getData());
    }
}
