<?php

declare(strict_types=1);

namespace App\Form\DataMapper;

use App\Entity\Rank;
use Symfony\Component\Form\DataMapperInterface;

class RankDataMapper implements DataMapperInterface
{
    /**
     * {@inheritdoc}
     */
    public function mapDataToForms(mixed $viewData, \Traversable $forms)
    {
        $forms = iterator_to_array($forms);
        if ($viewData instanceof Rank) {
            $forms['directorate']->setData($viewData->getDirectorate());
            $forms['name']->setData($viewData->getName());
        }
    }

    /**
     * {@inheritdoc}
     */
    public function mapFormsToData(\Traversable $forms, mixed &$viewData)
    {
        $forms = iterator_to_array($forms);

        if (!$viewData instanceof Rank) {
            $viewData = new Rank($forms['directorate']->getData());
        }
        $viewData->setName($forms['name']->getData());
    }
}
