<?php

declare(strict_types=1);

namespace App\Form\DataMapper;

use App\Entity\Employee;
use Symfony\Component\Form\DataMapperInterface;

class EmployeeDataMapper implements DataMapperInterface
{
    /**
     * {@inheritDoc}
     */
    public function mapDataToForms(mixed $viewData, \Traversable $forms)
    {
        $forms = iterator_to_array($forms);
        if ($viewData instanceof Employee) {
            $forms['directorate']->setData($viewData->getDirectorate());
            $forms['user']->setData($viewData->getUser());
            $forms['number']->setData($viewData->getNumber());
            $forms['position']->setData($viewData->getPosition());
            $forms['rank']->setData($viewData->getRank());
        }
    }

    /**
     * {@inheritDoc}
     */
    public function mapFormsToData(\Traversable $forms, mixed &$viewData)
    {
        $forms = iterator_to_array($forms);
        if (!$viewData instanceof Employee) {
            $viewData = new Employee($forms['user']->getData(), $forms['directorate']->getData());
        }
        $viewData->setNumber($forms['number']->getData());
        $viewData->setPosition($forms['position']->getData());
        $viewData->setRank($forms['rank']->getData());
    }
}
