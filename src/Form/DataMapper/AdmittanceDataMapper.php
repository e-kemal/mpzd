<?php

declare(strict_types=1);

namespace App\Form\DataMapper;

use App\Entity\Admittance;
use Symfony\Component\Form\DataMapperInterface;

class AdmittanceDataMapper implements DataMapperInterface
{
    public function mapDataToForms(mixed $viewData, \Traversable $forms)
    {
        $forms = iterator_to_array($forms);
        if ($viewData instanceof Admittance) {
            $forms['directorate']->setData($viewData->getDirectorate());
            $forms['name']->setData($viewData->getName());
        }
    }

    public function mapFormsToData(\Traversable $forms, mixed &$viewData)
    {
        $forms = iterator_to_array($forms);

        if (!$viewData instanceof Admittance) {
            $viewData = new Admittance($forms['directorate']->getData());
        }
        $viewData->setName($forms['name']->getData());
    }
}
